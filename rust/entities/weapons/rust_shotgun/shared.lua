-- Variables that are used on both client and server
SWEP.Gun = ("rust_shotgun") -- must be the name of your swep but NO CAPITALS!
SWEP.Category				= "Pook's Rust Weapons"
SWEP.Author				= "Pook"
SWEP.Contact				= ""
SWEP.Purpose				= ""
SWEP.Instructions				= ""
SWEP.MuzzleAttachment			= "1" 	-- Should be "1" for CSS models or "muzzle" for hl2 models
SWEP.ShellEjectAttachment			= "2" 	-- Should be "2" for CSS models or "1" for hl2 models
SWEP.PrintName				= "Shotgun"		-- Weapon name (Shown on HUD)	
SWEP.Slot				= 2				-- Slot in the weapon selection menu
SWEP.SlotPos				= 2			-- Position in the slot
SWEP.DrawAmmo				= true		-- Should draw the default HL2 ammo counter
SWEP.DrawWeaponInfoBox			= false		-- Should draw the weapon info box
SWEP.BounceWeaponIcon   		= 	false	-- Should the weapon icon bounce?
SWEP.DrawCrosshair			= false		-- set false if you want no crosshair
SWEP.Weight				= 30			-- rank relative ot other weapons. bigger is better
SWEP.AutoSwitchTo			= true		-- Auto switch to if we pick it up
SWEP.AutoSwitchFrom			= true		-- Auto switch from if you pick up a better weapon
SWEP.HoldType 				= "shotgun"		-- how others view you carrying the weapon
-- normal melee melee2 fist knife smg ar2 pistol rpg physgun grenade shotgun crossbow slam passive 
-- you're mostly going to use ar2, smg, shotgun or pistol. rpg and crossbow make for good sniper rifles

SWEP.ViewModelFOV			= 98

SWEP.ViewModelFlip			= false
SWEP.ViewModel				= "models/weapons/v_6fp.mdl"	-- Weapon view model
SWEP.WorldModel				= "models/weapons/w_6fp.mdl"	-- Weapon world model
SWEP.Base				= "bobs_gun_base"
SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true
SWEP.FiresUnderwater = true
SWEP.ShowWorldModel = true
SWEP.UseHands = true

SWEP.Primary.Sound			= Sound("fp6.single")		-- Script that calls the primary fire sound	
SWEP.Primary.RPM			= 54			-- This is in Rounds Per Minute
SWEP.Primary.ClipSize			= 8		-- Size of a clip
SWEP.Primary.DefaultClip		= 64		-- Bullets you start with
SWEP.Primary.KickUp				= 1.25		-- Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.8		-- Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.3		-- Maximum up recoil (stock)
SWEP.Primary.Automatic			= false		-- Automatic = true; Semi Auto = false
SWEP.Primary.Ammo			= "buckshot"			-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets

SWEP.SelectiveFire		= false

SWEP.Secondary.IronFOV			= 45		-- How much you 'zoom' in. Less is more! 	

SWEP.data 				= {}				--The starting firemode
SWEP.data.ironsights			= 1

SWEP.Primary.NumShots	= 9		-- How many bullets to shoot per trigger pull
SWEP.Primary.Damage		= 14	-- Base damage per bullet
SWEP.Primary.Spread		= .055	-- Define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy = .053 -- Ironsight accuracy, should be the same for shotguns

-- Enter iron sight info and bone mod info below
SWEP.IronSightsPos = Vector(-4.415, 1.366, 2.48)
SWEP.IronSightsAng = Vector(-0.932, -1.78, 0)

SWEP.SightsPos = Vector(-4.415, 1.366, 2.48)
SWEP.SightsAng = Vector(-0.932, -1.78, 0)

SWEP.RunSightsPos = Vector(5.203, -4.041, 0.259)
SWEP.RunSightsAng = Vector(-20.354, 51.986, -13.731)
SWEP.ViewModelBoneMods = {
	["ValveBiped.FP6_base"] = { scale = Vector(1, 1, 1), pos = Vector(-9.398, 0.5, -5.804), angle = Angle(-1.359, 0, -1.864) },
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(-7.398, 0.5, -6), angle = Angle(1.284, 0, 0) }
}



function SWEP:PrimaryAttack()
	if self:CanPrimaryAttack() and self.Owner:IsPlayer() then
	if !self.Owner:KeyDown(IN_SPEED) and !self.Owner:KeyDown(IN_RELOAD) then
		self:ShootBulletInformation()
		self.Weapon:TakePrimaryAmmo(1)
		
		if self.Silenced then
			self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_SILENCED )
			self.Weapon:EmitSound(self.Primary.SilencedSound)
		else
			self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_1)
			self.Weapon:EmitSound(self.Primary.Sound)
		end	
	
		local fx 		= EffectData()
		fx:SetEntity(self.Weapon)
		fx:SetOrigin(self.Owner:GetShootPos())
		fx:SetNormal(self.Owner:GetAimVector())
		fx:SetAttachment(self.MuzzleAttachment)
		if GetConVar("M9KGasEffect") != nil then
			if GetConVar("M9KGasEffect"):GetBool() then 
				util.Effect("m9k_rg_muzzle_rifle",fx)
			end
		end
		self.Owner:SetAnimation( PLAYER_ATTACK1 )
		self.Owner:MuzzleFlash()
		self.Weapon:SetNextPrimaryFire(CurTime()+1/(self.Primary.RPM/60))
		self:CheckWeaponsAndAmmo()
		self.RicochetCoin = (math.random(1,4))
		if self.BoltAction then self:BoltBack() end
	end
	elseif self:CanPrimaryAttack() and self.Owner:IsNPC() then
		self:ShootBulletInformation()
		self.Weapon:TakePrimaryAmmo(1)
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
		self.Weapon:EmitSound(self.Primary.Sound)
		self.Owner:SetAnimation( PLAYER_ATTACK1 )
		self.Owner:MuzzleFlash()
		self.Weapon:SetNextPrimaryFire(CurTime()+1/(self.Primary.RPM/60))
		self.RicochetCoin = (math.random(1,4))
	end
end

function SWEP:Reload()
	
		self.Weapon:Reload1()
		
		timer.Simple(0.6, function() self.Weapon:Reload2()end)
		
		timer.Simple(1.6, function() self.Weapon:Reload3()end)
		
		
		
end

function SWEP:Reload1()
	self.Weapon:DefaultReload(ACT_SHOTGUN_RELOAD_START)
	
end

function SWEP:Reload2()
	self.Weapon:DefaultReload(ACT_VM_RELOAD)
	
	self.Owner:GetViewModel():SetPlaybackRate(0.8)
	
end

function SWEP:Reload3()
	self.Weapon:DefaultReload(ACT_SHOTGUN_RELOAD_FINISH)
	self.Owner:GetViewModel():SetPlaybackRate(0.8)
end


