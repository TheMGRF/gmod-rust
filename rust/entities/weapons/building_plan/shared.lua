AddCSLuaFile()

SWEP.PrintName = "Building Plan"
SWEP.Slot = 0
SWEP.SlotPos = 1
SWEP.DrawAmmo = false
SWEP.DrawCrosshair = true

SWEP.Author = GAMEMODE.Author
SWEP.Instructions = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Category = RUST.Config.Category
SWEP.AnimPrefix = "rpg"

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.ViewModel = Model("models/weapons/c_arms.mdl")
SWEP.WorldModel = Model("models/blacksnow/smallstash.mdl")

SWEP.Spawnable = true
SWEP.AdminOnly = true

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
local multi = 500

local ALIGN_TOP = 1
local ALIGN_MIDDLE = 2
local ALIGN_BOTTOM = 3

function WallCollisionCheck(ent, snappingEnt, snapType, hit)
	local tmpVec1 = ent:OBBCenter()
	tmpVec1.y = ent:OBBMaxs().y
	tmpVec1 = ent:LocalToWorld(tmpVec1)
	local tr1 = util.TraceLine({start = tmpVec1, endpos = tmpVec1 + Vector(0, 1, 0), filter = ent})

	local tmpVec2 = ent:OBBCenter()
	tmpVec2.y = ent:OBBMins().y
	tmpVec2 = ent:LocalToWorld(tmpVec2)
	local tr2 = util.TraceLine({start = tmpVec2, endpos = tmpVec2 + Vector(0, -1, 0), filter = ent})

	if (tr1.Entity:GetClass() != "wood_pillar" || tr2.Entity:GetClass() != "wood_pillar") then
		return true
	end

	local tr = util.TraceHull({
		start = ent:GetPos(),
		endpos = ent:GetPos(),
		filter = {tr1.Entity, tr2.Entity, snappingEnt, ent},
		mins = ent:OBBMins(),
		maxs = ent:OBBMaxs()
	})

	return tr.Hit
end

local models = {
	{name = "Foundation", model = "models/vasey/wood_house/wood_foundation.mdl", ent = "wood_foundation", align = ALIGN_TOP, snapType = {1}, freePlace = true, forceBottom = false},
	{name = "Pillar", model = "models/vasey/wood_house/wood_pillar.mdl", ent = "wood_pillar", align = ALIGN_BOTTOM, snapType = {3, 5, 6}, freePlace = false, forceBottom = true},
	{name = "Doorway", model = "models/vasey/wood_house/wood_doorway.mdl", ent = "wood_doorway", align = ALIGN_MIDDLE, snapType = {2}, freePlace = false, forceBottom = false, checkCollisions = WallCollisionCheck},
	{name = "Ramp", model = "models/vasey/wood_house/wood_ramp.mdl", ent = "wood_ramp", align = ALIGN_MIDDLE, snapType = {1}, freePlace = false, forceBottom = false},
	{name = "Wall", model = "models/vasey/wood_house/wood_wall.mdl", ent = "wood_wall", align = ALIGN_MIDDLE, snapType = {2}, freePlace = false, forceBottom = false, checkCollisions = WallCollisionCheck},
	{name = "Window", model = "models/vasey/wood_house/wood_window.mdl", ent = "wood_window", align = ALIGN_MIDDLE, snapType = {2}, freePlace = false, forceBottom = false, checkCollisions = WallCollisionCheck},
	{name = "Stairs", model = "models/vasey/wood_house/wood_stairs.mdl", ent = "wood_stairs", align = ALIGN_MIDDLE, snapType = {4}, freePlace = false, forceBottom = false},
	{name = "Ceiling", model = "models/vasey/wood_house/wood_ceiling.mdl", ent = "wood_ceiling", align = ALIGN_MIDDLE, snapType = {4}, freePlace = false, forceBottom = false},
}


if SERVER then
	util.AddNetworkString("SelectNewModel")

	net.Receive("SelectNewModel", function(len, ply)
		local num = net.ReadInt(5)
		local wep = ply:GetActiveWeapon()
		if (wep:GetClass() == "building_plan") then
			wep:SetSelectedModelIndex(num)
		end
	end)
end

function SWEP:Initialize()
	self:SetHoldType("grenade")
	self:SetSelectedModelIndex(1)
end

function SWEP:PrimaryAttack()
	if !IsFirstTimePredicted() then return end
	if CLIENT then return end
	local ply = self.Owner

	local shootpos = ply:GetShootPos()
	local endshootpos = shootpos + ply:GetAimVector() * multi
	local tmin = Vector(1, 1, 1) * -10
	local tmax = Vector(1, 1, 1) * 10

	local tr = util.TraceHull({
		start = shootpos,
		endpos = endshootpos,
		filter = ply,
		mask = MASK_SHOT_HULL,
		mins = tmin,
		maxs = tmax
	})

	if (!IsValid(tr.Entity)) then
		tr = util.TraceLine({
			start = shootpos,
			endpos = endshootpos,
			filter = ply,
			mask = MASK_SHOT_HULL
		})
	end
	local selModel = models[self:GetSelectedModelIndex()]
	local ent = ents.Create(selModel.ent)
	ent:SetPos(tr.HitPos)
	ent:SetAngles(Angle(0, self.yRotation || 0, 0))
	ent:Spawn()

	ent:SetPos(tr.HitPos-ent:OBBCenter())
	if (selModel.align == ALIGN_TOP) then
		local tmpPos = ent:OBBCenter()
		tmpPos.z = ent:OBBMaxs().z - 10
		ent:SetPos(tr.HitPos-tmpPos)
	elseif (selModel.align == ALIGN_MIDDLE) then
		ent:SetPos(tr.HitPos-ent:OBBCenter())
	elseif (selModel.align == ALIGN_BOTTOM) then
		local tmpPos = ent:OBBCenter()
		tmpPos.z = ent:OBBMins().z
		ent:SetPos(tr.HitPos-tmpPos)
	end

	local hasSnapped = false
	local snapPointType = -1

	if (tr.Entity && IsValid(tr.Entity) && string.StartWith(tr.Entity:GetClass(), "wood_")) then
		local closest = tr.Entity:GetPos()
		local closestDist = 1000000

		local snapPoints = tr.Entity:GetSnapPoints() || {}

		for k,v in pairs(snapPoints) do
			if (table.HasValue(selModel.snapType, v.type)) then
				local dist = tr.HitPos:Distance(tr.Entity:LocalToWorld(v.pos))
				if (closestDist > dist) then
					snapPointType = v.type
					closest = v.pos
					closestDist = dist
				end
			end
		end

		local tmpPos = Vector(0, 0, 0)
		if (selModel.forceBottom) then
			tmpPos = ent:OBBCenter()
			tmpPos.z = ent:OBBMins().z
		end

		ent:SetPos(tr.Entity:LocalToWorld(closest) - tmpPos)

		hasSnapped = true
	end

	local tr1 = util.TraceHull({start = ent:GetPos(), endpos = ent:GetPos(), mask = MASK_NPCWORLDSTATIC, mins = ent:OBBMins(), maxs = ent:OBBMaxs()})
	local tr2 = util.TraceHull({start = ent:GetPos(), endpos = ent:GetPos(), filter = ent, ignoreworld = true, mask = MASK_SHOT_HULL, maxs = ent:OBBMaxs()})

	local foundationCheck = true

	if (models[self:GetSelectedModelIndex()].ent == "wood_foundation") then
		foundationCheck = tr1.Hit
	end

	local collisionCheck = tr2.Hit

	if (collisionCheck == true && models[self:GetSelectedModelIndex()].checkCollisions) then
		collisionCheck = models[self:GetSelectedModelIndex()].checkCollisions(ent, tr.Entity, snapPointType, tr.Hit)
	end

	if (collisionCheck || !(hasSnapped && foundationCheck) && !(models[self:GetSelectedModelIndex()].freePlace && tr1.Hit && !tr2.Hit)) then
		ent:Remove()
	end
end

function SWEP:SecondaryAttack()
	if !IsFirstTimePredicted() then return end
	if SERVER then return end

	local modelMenu = DermaMenu()
	for k,v in pairs(models) do
		modelMenu:AddOption(v.name, function()
			net.Start("SelectNewModel")
				net.WriteInt(k, 5)
			net.SendToServer()
		end)
	end
	modelMenu:Open(100, 100)

	timer.Simple(0, function() gui.SetMousePos(110, 110) end)
end

function SWEP:Reload()
	return true
end

if CLIENT then
	local bagModel = ClientsideModel("models/blacksnow/smallstash.mdl")
	bagModel:SetNoDraw(true)

	function SWEP:DrawWorldModel()
		local attach_id = self.Owner:LookupAttachment("anim_attachment_RH")
		if !attach_id then return end

		local attach = self.Owner:GetAttachment(attach_id)

		if !attach then return end

		local pos = attach.Pos
		local ang = attach.Ang

		bagModel:SetModelScale(1, 0)
		pos = pos + (ang:Forward() * 0.5)
		pos = pos + (ang:Up() * -7)

		bagModel:SetPos(pos)
		bagModel:SetAngles(ang)

		bagModel:SetRenderOrigin(pos)
		bagModel:SetRenderAngles(ang)
		bagModel:SetupBones()
		bagModel:DrawModel()
		bagModel:SetRenderOrigin()
		bagModel:SetRenderAngles()
	end
end

function SWEP:AjustHolo()
	self.holo:SetModel(models[self:GetSelectedModelIndex()].model)

	self.holo:SetAngles(Angle(0, self.yRotation || 0, 0))

	local ply = self.Owner
	local shootpos = ply:GetShootPos()
	local endshootpos = shootpos + ply:GetAimVector() * multi
	local tmin = Vector(1, 1, 1) * -10
	local tmax = Vector(1, 1, 1) * 10

	local tr = util.TraceHull({start = shootpos, endpos = endshootpos, filter = ply, mask = MASK_SHOT_HULL, mins = tmin, maxs = tmax})

	if (!IsValid(tr.Entity)) then
		tr = util.TraceLine({start = shootpos, endpos = endshootpos, filter = ply, mask = MASK_SHOT_HULL})
	end


	self.holo:SetPos(tr.HitPos-self.holo:OBBCenter())
	if (models[self:GetSelectedModelIndex()].align == ALIGN_TOP) then
		local tmpPos = self.holo:OBBCenter()
		tmpPos.z = self.holo:OBBMaxs().z - 10
		self.holo:SetPos(tr.HitPos-tmpPos)
	elseif (models[self:GetSelectedModelIndex()].align == ALIGN_MIDDLE) then
		self.holo:SetPos(tr.HitPos-self.holo:OBBCenter())
	elseif (models[self:GetSelectedModelIndex()].align == ALIGN_BOTTOM) then
		local tmpPos = self.holo:OBBCenter()
		tmpPos.z = self.holo:OBBMins().z
		self.holo:SetPos(tr.HitPos-tmpPos)
	end

	local hasSnapped = false
	local snapPointType = -1

	if (tr.Entity && IsValid(tr.Entity) && string.StartWith(tr.Entity:GetClass(), "wood_")) then
		local snapPoints = {}
		if (tr.Entity.GetSnapPoints) then
			snapPoints = tr.Entity:GetSnapPoints()
		end

		local closest = tr.Entity:GetPos()
		local closestDist = 1000000

		for k,v in pairs(snapPoints) do
			if (table.HasValue(models[self:GetSelectedModelIndex()].snapType, v.type)) then
				local dist = tr.HitPos:Distance(tr.Entity:LocalToWorld(v.pos))
				if (closestDist > dist) then
					snapPointType = v.type
					closest = v.pos
					closestDist = dist
				end
			end
		end

		if (closestDist != 1000000) then
			local tmpPos = Vector(0, 0, 0)
			if (models[self:GetSelectedModelIndex()].forceBottom) then
				tmpPos = self.holo:OBBCenter()
				tmpPos.z = self.holo:OBBMins().z
			end

			self.holo:SetPos(tr.Entity:LocalToWorld(closest) - tmpPos)
			hasSnapped = true
		end
	end

	local tr1 = util.TraceHull({start = self.holo:GetPos(), endpos = self.holo:GetPos(), mask = MASK_NPCWORLDSTATIC, mins = self.holo:OBBMins(), maxs = self.holo:OBBMaxs()})
	local tr2 = util.TraceHull({start = self.holo:GetPos(), endpos = self.holo:GetPos(), ignoreworld = true, mask = MASK_SHOT_HULL, maxs = self.holo:OBBMaxs()})

	local foundationCheck = true

	if (models[self:GetSelectedModelIndex()].ent == "wood_foundation") then
		foundationCheck = tr1.Hit
	end

	local collisionCheck = tr2.Hit

	if (collisionCheck == true && models[self:GetSelectedModelIndex()].checkCollisions) then
		collisionCheck = models[self:GetSelectedModelIndex()].checkCollisions(self.holo, tr.Entity, snapPointType, tr.Hit)
	end

	if (collisionCheck || !(hasSnapped && foundationCheck) && !(models[self:GetSelectedModelIndex()].freePlace && tr1.Hit && !tr2.Hit)) then
		self.holo:SetColor(Color(255, 0, 0, 200))
	else
		self.holo:SetColor(Color(0, 255, 0, 200))
	end
end

function SWEP:Deploy()
	if (CLIENT && !IsValid(self.holo)) then
		self.holo = ents.CreateClientProp()
		self.holo:SetCollisionGroup(COLLISION_GROUP_WORLD)
		self.holo:SetColor(Color(0, 255, 0, 200))
		self.holo:SetRenderMode(RENDERMODE_TRANSALPHA)
		self.holo:Spawn()
		self:AjustHolo()
	end

	self.yRotation = 0
	self.lastReload = 0
end

function SWEP:Holster()
	if (CLIENT && IsValid(self.holo)) then
		self.holo:Remove()
	end
	return true
end

function SWEP:OnDrop()
	if (CLIENT && IsValid(self.holo)) then
		self.holo:Remove()
	end
end

function SWEP:OnRemove()
	if (CLIENT && IsValid(self.holo)) then
		self.holo:Remove()
	end
end

function SWEP:Think()
	if (CLIENT && IsValid(self.holo)) then
		self:AjustHolo()
	end
end

function SWEP:SetupDataTables()
	self:NetworkVar("Int", 0, "SelectedModelIndex")
end

function SWEP:Reload()
	if self.lastReload > CurTime() then return end

	self.yRotation = self.yRotation + 90

	self.lastReload = CurTime() + 0.2
end