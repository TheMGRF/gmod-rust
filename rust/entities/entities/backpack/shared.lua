ENT.Base 					= "base_entity" 
ENT.Type 					= "anim"
ENT.PrintName				= "Backpack"
ENT.Author					= GAMEMODE.Author
ENT.AutomaticFrameAdvance 	= true
ENT.Spawnable 				= false
ENT.AdminSpawnable 			= false
ENT.Category 				= RUST.Config.Category

function ENT:SetAutomaticFrameAdvance( bUsingAnim )
	self.AutomaticFrameAdvance = bUsingAnim
end

function ENT:SetupDataTables()
	self:NetworkVar( "String", 0, "Items" );
end