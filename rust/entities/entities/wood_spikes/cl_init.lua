include( 'shared.lua' )
 
ENT.RenderGroup = RENDERGROUP_BOTH
local MAT = Material("models/wireframe")

function ENT:Draw()
	self:DrawModel()
	if GetConVar("developer"):GetInt() >= 2 then
		render.DrawWireframeBox(self:GetPos(), self:GetAngles(), self:OBBMins(), self:OBBMaxs(), Color(255, 0, 0))
	end
end