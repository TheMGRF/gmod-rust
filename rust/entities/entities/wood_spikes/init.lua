AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include('shared.lua')

function ENT:Initialize()
	self:SetModel("models/blacksnow/spikes.mdl")
	self:SetColor(Color(255,255,255))
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetUseType(SIMPLE_USE);
	local PhysAwake = self.Entity:GetPhysicsObject()
	if PhysAwake:IsValid() then
		--PhysAwake:Wake()
		PhysAwake:EnableMotion(false)
	end
	self:SetHealth(300)
	self:SetMaxHealth(300)
end

function ENT:OnTakeDamage( dmg ) 
	self:TakeDMG(dmg)
end

function ENT:Touch(ent)
	if ent:IsPlayer() then 
		ent:TakeDamage(1, self, self)
	end
end