AddCSLuaFile()
ENT.Base = "wood_base"
ENT.PrintName				= "Wooden Ramp"
ENT.Spawnable 				= true
ENT.AdminSpawnable 			= true
ENT.Author					= GAMEMODE.Author
ENT.Category 				= RUST.Config.Category
ENT.Mdl = "models/vasey/wood_house/wood_ramp.mdl"
ENT.SHlt = 250

function ENT:GetSnapPoints()
	local tab = {}

	/*
	//Corner
	local tmpVec = self:OBBMaxs()
	tmpVec.x = self:OBBMins().x
	table.insert(tab, {type = 3, pos = tmpVec})

	//Corner
	local tmpVec = self:OBBMaxs()
	tmpVec.x = self:OBBMins().x
	tmpVec.y = self:OBBMins().y
	table.insert(tab, {type = 3, pos = tmpVec})
	*/
	


	//Mid
	local tmpVec = self:OBBMaxs()
	tmpVec.y = 0
	tmpVec.x = self:OBBMins().x
	table.insert(tab, {type = 2, pos = tmpVec})



	//Next to
	local tmpVec = self:OBBMins()
	tmpVec.y = 0
	tmpVec.x = tmpVec.x*2
	table.insert(tab, {type = 1, pos = tmpVec})

	//Next to
	local tmpVec = self:OBBMins()
	tmpVec.x = 0
	tmpVec.y = tmpVec.y*2
	table.insert(tab, {type = 1, pos = tmpVec})

	//Next to
	local tmpVec = self:OBBMaxs()
	tmpVec.z = 0
	tmpVec.x = 0
	tmpVec.y = tmpVec.y*2
	table.insert(tab, {type = 1, pos = tmpVec})

	return tab
end