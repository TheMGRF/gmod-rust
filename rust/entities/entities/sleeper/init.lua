AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include('shared.lua')

function ENT:Initialize()
	self:SetModel(RUST.Config.Model)
	self:SetColor(Color(255,255,255))
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_NONE)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE);
	local PhysAwake = self.Entity:GetPhysicsObject()
	if PhysAwake:IsValid() then
		PhysAwake:Wake()
	end
	self:SetHealth(100)
	self:SetMaxHealth(100)
	self:SetAngles(Angle(-90,0,90))
end

function ENT:OnTakeDamage(dmg) 
	// some other code
		// doesnt register in the body
	if self:TakeDMG(dmg, true) then
		local ent = ents.Create("backpack")
		ent:SetPos(self:GetPos())
		ent:Spawn()
		--ent:SetItems(self:GetItems())
		ChangeSleeperStats(self:GetplyID(), nil, Vector(0,0,0), true, dmg:GetAttacker():GetName().." killed you while you were sleeping.")
	end
end