include( 'shared.lua' )
 
ENT.RenderGroup = RENDERGROUP_BOTH

function ENT:Initialize()
end

function ENT:Draw()
	self:DrawModel()
end

function ENT:Think()
    local dlight = DynamicLight( self:EntIndex() )
	if ( dlight ) then
		local r, g, b, a = self:GetColor()
		dlight.Pos = self:GetPos()
		dlight.r = 227
		dlight.g = 140
		dlight.b = 45
		dlight.Brightness = 2.5
		dlight.Size = 700
		dlight.Decay = 1000
		dlight.DieTime = 1000
	end
end