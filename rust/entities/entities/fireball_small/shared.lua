ENT.Base 					= "base_entity" 
ENT.Type 					= "anim"
ENT.PrintName				= "Fireball"
ENT.Author					= GAMEMODE.Author
ENT.AutomaticFrameAdvance 	= true
ENT.Spawnable 				= true
ENT.AdminSpawnable 			= true
ENT.Category 				= RUST.Config.Category

function ENT:SetAutomaticFrameAdvance( bUsingAnim )
	self.AutomaticFrameAdvance = bUsingAnim
end