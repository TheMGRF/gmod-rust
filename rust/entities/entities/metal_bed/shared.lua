ENT.Base 					= "base_entity" 
ENT.Type 					= "anim"
ENT.PrintName				= "Sleeping Bag"
ENT.Author					= GAMEMODE.Author
ENT.AutomaticFrameAdvance 	= true
ENT.Spawnable 				= true
ENT.AdminSpawnable 			= true
ENT.Category 				= RUST.Config.Category

function ENT:SetAutomaticFrameAdvance( bUsingAnim )
	self.AutomaticFrameAdvance = bUsingAnim
end

function ENT:SetupDataTables()
	self:NetworkVar( "String", 0, "plyID" );
	self:NetworkVar( "Float", 0, "Cooldown" );
end