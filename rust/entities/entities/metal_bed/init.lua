AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include( 'shared.lua' )

function ENT:Initialize( )
	self:SetModel("models/blacksnow/sleepbag.mdl")
	self:SetColor(Color(255,255,255))
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetUseType(SIMPLE_USE);
	local PhysAwake = self.Entity:GetPhysicsObject( )
	if PhysAwake:IsValid() then
		--PhysAwake:Wake()
		PhysAwake:EnableMotion(false)
	end
	self:SetHealth(100)
	self:SetMaxHealth(100)
	self:SetCooldown(CurTime()+RUST.Config.SleepCooldown)
end

function ENT:OnTakeDamage( dmg ) 
	self:TakeDMG(dmg, true)
end

function ENT:Use(ply)
	if ply:SteamID() == self:GetplyID() then
		ply.bag = nil
		self:Remove()
	end
end