AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/blacksnow/furnace.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_NONE)
	self:SetSolid(SOLID_VPHYSICS)
	local phys = self:GetPhysicsObject()
	if phys and phys:IsValid() then
		phys:Wake()
	end
	self.timeleft = CurTime() + 10
	self.fireball = nil
	self:Cook()
end

function ENT:Cook()
	self.fireball = ents.Create("fireball_small")
	self.fireball:SetPos(self:GetPos()-Vector(0, 0, 0))
	self.fireball:SetParent(self)
	self.fireball:Activate()
	self.fireball:Spawn()
end

function ENT:StopCooking()
	if IsValid(self.fireball) then 
		self.fireball:Remove()
	end
end

function ENT:PhysicsCollide(data, physobj)
end

function ENT:Use(ply)
end