AddCSLuaFile()
ENT.Base = "wood_base"
ENT.PrintName				= "Wooden Pillar"
ENT.Spawnable 				= true
ENT.AdminSpawnable 			= true
ENT.Author					= GAMEMODE.Author
ENT.Category 				= RUST.Config.Category
ENT.Mdl = "models/vasey/wood_house/wood_pillar.mdl"
ENT.SHlt = 750

function ENT:GetSnapPoints()
	local tab = {}

	//Center
	local tmpVec = self:OBBCenter()
	tmpVec.z = self:OBBMaxs().z
	table.insert(tab, {type = 3, pos = tmpVec})

	return tab
end