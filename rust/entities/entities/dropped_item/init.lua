AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include('shared.lua')

function ENT:Initialize()
	self:SetModel("models/blacksnow/smallstash.mdl")
	self:SetColor(Color(255,255,255))
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE);
	local PhysAwake = self.Entity:GetPhysicsObject()
	if PhysAwake:IsValid() then
		PhysAwake:Wake()
		PhysAwake:EnableMotion(false)
	end
end

function ENT:Use(ply)
	local extra = self:Getextra() or nil
	if extra == "" or extra == nil then
		extra = nil
	else
		extra = util.JSONToTable(extra)
	end
	ply:GiveItem(self:GetItmName(), self:GetItmAmt(), extra)
	self:Remove()
end