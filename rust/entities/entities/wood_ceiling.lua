AddCSLuaFile()
ENT.Base = "wood_base"
ENT.PrintName				= "Wooden Ceiling"
ENT.Spawnable 				= true
ENT.AdminSpawnable 			= true
ENT.Author					= GAMEMODE.Author
ENT.Category 				= RUST.Config.Category
ENT.Mdl = "models/vasey/wood_house/wood_ceiling.mdl"
ENT.SHlt = 750

function ENT:GetSnapPoints()
	local tab = {}

	/*
	//Corner
	table.insert(tab, {type = 3, pos = self:OBBMaxs()})

	//Corner
	local tmpVec = self:OBBMaxs()
	tmpVec.x = self:OBBMins().x
	table.insert(tab, {type = 3, pos = tmpVec})

	//Corner
	local tmpVec = self:OBBMaxs()
	tmpVec.y = self:OBBMins().y
	table.insert(tab, {type = 3, pos = tmpVec})

	//Corner
	local tmpVec = self:OBBMaxs()
	tmpVec.x = self:OBBMins().x
	tmpVec.y = self:OBBMins().y
	table.insert(tab, {type = 3, pos = tmpVec})
	*/



	//Mid
	local tmpVec = self:OBBMaxs()
	tmpVec.y = 0
	table.insert(tab, {type = 2, pos = tmpVec})

	//Mid
	local tmpVec = self:OBBMaxs()
	tmpVec.x = 0
	table.insert(tab, {type = 2, pos = tmpVec})

	//Mid
	local tmpVec = self:OBBMaxs()
	tmpVec.x = 0
	tmpVec.y = self:OBBMins().y
	table.insert(tab, {type = 2, pos = tmpVec})

	//Mid
	local tmpVec = self:OBBMaxs()
	tmpVec.y = 0
	tmpVec.x = self:OBBMins().x
	table.insert(tab, {type = 2, pos = tmpVec})



	//Center
	local tmpVec = self:OBBCenter()
	tmpVec.z = self:OBBMaxs().z
	table.insert(tab, {type = 4, pos = tmpVec})

	return tab
end