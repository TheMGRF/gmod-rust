AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include( 'shared.lua' )

function ENT:Initialize( )
	self:SetModel("models/vasey/wood_house/door.mdl")
	self:SetColor(Color(255,255,255))
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetUseType(SIMPLE_USE);
	local PhysAwake = self.Entity:GetPhysicsObject( )
	if PhysAwake:IsValid() then
		--PhysAwake:Wake()
		PhysAwake:EnableMotion(false)
	end
	self:SetHealth(200)
	self:SetMaxHealth(200)

	timer.Simple(.1, function()
		self.OPos = self:GetPos()
		self.OAng = self:GetAngles()
		self.OpenPos = self:LocalToWorld(Vector(-31.275,-31.15,-1))
		self.OpenAng = self:LocalToWorldAngles(Angle(0,90,0))
	end)

	self.FuturePos = nil
	self.FutureAng = nil

	self.Opened = false
	self:SetCode(99999)
end

function ENT:OnTakeDamage(dmg) 
	self:TakeDMG(dmg)
end

function ENT:Use(ply)
	if ply:SteamID() != self:GetplyID() then ply:Hint("This door is locked!") return end
	if self.FuturePos != nil then return end
	if self.Opened then
		// Closed
		self.FuturePos = self.OPos
		self.FutureAng = self.OAng
		self.Opened = false
		self:EmitSound("rust_door_closed")
	else
		// Opened
		self.FuturePos = self.OpenPos
		self.FutureAng = self.OpenAng
		self.Opened = true
		self:EmitSound("rust_door_opened")
	end
end

local num = 1
local num2 = 1
function ENT:Think()
	if self.FuturePos != nil then
		local pos = self:GetPos()

		pos.x = math.Approach(pos.x, self.FuturePos.x, num)
		pos.y = math.Approach(pos.y, self.FuturePos.y, num)
		pos.z = math.Approach(pos.z, self.FuturePos.z, num)

		if pos == self.FuturePos then
			self:SetPos(self.FuturePos)
			self.FuturePos = nil
		else
			self:SetPos(pos)
		end
	end
	if self.FutureAng != nil then
		local ang = self:GetAngles()

		ang.pitch = math.Approach(ang.pitch, self.FutureAng.pitch, num2)
		ang.yaw = math.Approach(ang.yaw, self.FutureAng.yaw, num2)
		ang.roll = math.Approach(ang.roll, self.FutureAng.roll, num2)

		if ang == self.FutureAng then
			self:SetAngles(self.FutureAng)
			self.FutureAng = nil
		else
			self:SetAngles(ang)
		end
	end
end

/*concommand.Add("dab", function()
	for k,v in pairs(ents.GetAll()) do
		if v:GetClass() != "metal_door" then continue end
		print("OPos: "..tostring(v.OPos))
		print("OAng: "..tostring(v.OAng))
		print("Pos: "..tostring(v:GetPos()))
		print("Ang: "..tostring(v:GetAngles()))
	end
end)*/