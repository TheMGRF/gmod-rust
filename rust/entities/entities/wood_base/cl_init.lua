include("shared.lua")

ENT.RenderGroup = RENDERGROUP_BOTH

function ENT:Draw()
	self:DrawModel()
	if GetConVar("developer"):GetInt() >= 2 then
		render.DrawWireframeBox(self:GetPos(), self:GetAngles(), self:OBBMins(), self:OBBMaxs(), Color(255, 0, 0))

		local snapPoints = self:GetSnapPoints() || {}
		for k,v in pairs(snapPoints) do
			render.DrawWireframeSphere(self:LocalToWorld(v.pos), 2, 10, 10, Color(255, 255 / v.type, 0))
		end
	end
end