ENT.Base 					= "base_entity"
ENT.Type 					= "anim"
ENT.PrintName				= "Wooden"
ENT.Author					= GAMEMODE.Author
ENT.AutomaticFrameAdvance 	= true
ENT.Category 				= RUST.Config.Category

function ENT:SetAutomaticFrameAdvance( bUsingAnim )
	self.AutomaticFrameAdvance = bUsingAnim
end

function ENT:GetSnapPoints()
	local tab = {}

	//Corner
	table.insert(tab, {type = 3, pos = self:OBBMaxs()})

	//Corner
	local tmpVec = self:OBBMaxs()
	tmpVec.x = self:OBBMins().x
	table.insert(tab, {type = 3, pos = tmpVec})

	//Corner
	local tmpVec = self:OBBMaxs()
	tmpVec.y = self:OBBMins().y
	table.insert(tab, {type = 3, pos = tmpVec})

	//Corner
	local tmpVec = self:OBBMaxs()
	tmpVec.x = self:OBBMins().x
	tmpVec.y = self:OBBMins().y
	table.insert(tab, {type = 3, pos = tmpVec})



	//Mid
	local tmpVec = self:OBBMaxs()
	tmpVec.y = 0
	table.insert(tab, {type = 2, pos = tmpVec})

	table.insert(tab, {type = 5, pos = tmpVec})

	//Mid
	local tmpVec = self:OBBMaxs()
	tmpVec.x = 0
	table.insert(tab, {type = 2, pos = tmpVec})

	table.insert(tab, {type = 5, pos = tmpVec})

	//Mid
	local tmpVec = self:OBBMaxs()
	tmpVec.x = 0
	tmpVec.y = self:OBBMins().y
	table.insert(tab, {type = 2, pos = tmpVec})

	table.insert(tab, {type = 5, pos = tmpVec})

	//Mid
	local tmpVec = self:OBBMaxs()
	tmpVec.y = 0
	tmpVec.x = self:OBBMins().x
	table.insert(tab, {type = 2, pos = tmpVec})

	table.insert(tab, {type = 5, pos = tmpVec})



	//Next to
	local tmpVec = self:OBBMins()
	tmpVec.y = 0
	tmpVec.x = tmpVec.x * 2
	table.insert(tab, {type = 1, pos = tmpVec})

	//Next to
	local tmpVec = self:OBBMins()
	tmpVec.x = 0
	tmpVec.y = tmpVec.y * 2
	table.insert(tab, {type = 1, pos = tmpVec})

	//Next to
	local tmpVec = self:OBBMaxs()
	tmpVec.z = 0
	tmpVec.x = 0
	tmpVec.y = tmpVec.y * 2
	table.insert(tab, {type = 1, pos = tmpVec})

	//Next to
	local tmpVec = self:OBBMaxs()
	tmpVec.z = 0
	tmpVec.y = 0
	tmpVec.x = tmpVec.x * 2
	table.insert(tab, {type = 1, pos = tmpVec})



	//Center
	local tmpVec = self:OBBCenter()
	tmpVec.z = self:OBBMaxs().z
	table.insert(tab, {type = 4, pos = tmpVec})

	table.insert(tab, {type = 5, pos = tmpVec})

	return tab
end