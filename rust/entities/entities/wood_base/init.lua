AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT:Initialize()
	self:SetModel(self.Mdl)
	self:SetColor(Color(255,255,255))
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE);
	local PhysAwake = self.Entity:GetPhysicsObject()
	if PhysAwake:IsValid() then
		--PhysAwake:Wake()
		PhysAwake:EnableMotion(false)
	end
	self:SetHealth(self.SHlt)
	self:SetMaxHealth(self.SHlt)
end

function ENT:OnTakeDamage(dmg)
	self:TakeDMG(dmg)
end