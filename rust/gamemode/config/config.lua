RUST.Config.Health = 100 // How much health you start with
RUST.Config.Model = "models/player/Group01/male_04.mdl"
RUST.Config.Category = "Skynet Rust"
RUST.Config.SleepCooldown = 240
RUST.Config.FallDamage = true // player take fall damage
RUST.Config.InjuryMinDMG = 40 // The admount of damage (HP) to activate injury
RUST.Config.InjuryTime = 20 // How long the player is injured for
RUST.Config.RunSpeed = 240
RUST.Config.WalkSpeed = 160
RUST.Config.SleepersEnabled = true // Wheter or not sleepers are spawned at players pos when logged off

RUST.Config.SFXReplace = {
	["player/footsteps/sand1.wav"] = "effect/sfx/foot/grass_01.wav",
	["player/footsteps/sand2.wav"] = "effect/sfx/foot/grass_02.wav",
	["player/footsteps/sand3.wav"] = "effect/sfx/foot/grass_03.wav",
	["player/footsteps/sand4.wav"] = "effect/sfx/foot/grass_04.wav",

	["player/footsteps/wood1.wav"] = "effect/sfx/foot/wood1.wav",
	["player/footsteps/wood2.wav"] = "effect/sfx/foot/wood2.wav",
	["player/footsteps/wood3.wav"] = "effect/sfx/foot/wood3.wav",
	["player/footsteps/wood4.wav"] = "effect/sfx/foot/wood4.wav",

	["physics/surfaces/sand_impact_bullet1.wav"] = "effect/sfx/impact/dirt1.wav",
	["physics/surfaces/sand_impact_bullet2.wav"] = "effect/sfx/impact/dirt2.wav",
	["physics/surfaces/sand_impact_bullet3.wav"] = "effect/sfx/impact/dirt3.wav",
	["physics/surfaces/sand_impact_bullet4.wav"] = "effect/sfx/impact/dirt4.wav",
	["physics/surfaces/sand_impact_bullet5.wav"] = "effect/sfx/impact/dirt4.wav",

	["physics/wood/wood_solid_impact_bullet1.wav"] = "effect/sfx/impact/wood1.wav",
	["physics/wood/wood_solid_impact_bullet2.wav"] = "effect/sfx/impact/wood2.wav",
	["physics/wood/wood_solid_impact_bullet3.wav"] = "effect/sfx/impact/wood3.wav",
	["physics/wood/wood_solid_impact_bullet4.wav"] = "effect/sfx/impact/wood4.wav",
	["physics/wood/wood_solid_impact_bullet5.wav"] = "effect/sfx/impact/wood5.wav",

	["physics/concrete/concrete_impact_bullet1.wav"] = "effect/sfx/impact/concrete1.wav",
	["physics/concrete/concrete_impact_bullet2.wav"] = "effect/sfx/impact/concrete2.wav",
	["physics/concrete/concrete_impact_bullet3.wav"] = "effect/sfx/impact/concrete3.wav",
	["physics/concrete/concrete_impact_bullet4.wav"] = "effect/sfx/impact/concrete4.wav",
	["physics/concrete/concrete_impact_bullet5.wav"] = "effect/sfx/impact/concrete5.wav",

	["ambient/fire/fire_small_loop2.wav"] = "",
	["common/wpn_moveselect.wav"] = "",
	["common/wpn_hudoff.wav"] = "",
	["items/ammo_pickup.wav"] = "",
}

RUST.Config.EntityImpactSoundBullet = {
	["wood_wall"] = "effect/sfx/impact/wood5.wav"
}

RUST.Config.HitGroups = {
	[HITGROUP_GENERIC] = "Unknown",
	[HITGROUP_HEAD] = "Head",
	[HITGROUP_CHEST] = "Body",
	[HITGROUP_STOMACH] = "Body",
	[HITGROUP_LEFTARM] = "Left Arm",
	[HITGROUP_RIGHTARM] = "Right Arm",
	[HITGROUP_LEFTLEG] = "Left Leg",
	[HITGROUP_RIGHTLEG] = "Right Leg",
	[HITGROUP_GEAR] = "Body",
}

RUST.Config.HitmarkerTime = 0.7 // How long the hitmarker is on screen

RUST.Config.colors = { -- These are the colors in the HUD, if you don't like a particular color or font you can edit it here.
	name = "Rust Default",
	font_regular = "Minecraftia 2.0",
	font_overhead = "Nina Bold",
	MainColor = Color( 46, 46, 46 ),
	Shadow = Color( 0, 0, 0, 50),
	HealthColor = Color( 0, 255, 0 ),
	FoodColor = Color( 135, 85, 0),
	White = Color( 225, 225, 225 ),
	Black = Color( 0, 0, 0 ),
	outline = Color( 0, 0, 0, 200)
}
RUST.Config.text = {
	lang = "EN - English",
	health = "HEALTH",
	food = "FOOD",
	rads = "RADS",
}
RUST.Config.playerInfo = true

RUST.Config.Inventory = {}
RUST.Inventory = {}
RUST.Config.Inventory.DefaultItems = { // ["itemid"] = amount
	["wood_planks"] = 10,
	["cooked_chicken_breast"] = 40,
	["rust_m4"] = 2,
	["building_plan"] = 1,
}
RUST.Config.Inventory.Slots = {
	["c1"] = true,
	["c2"] = true,
	["c3"] = true,
	["c4"] = true,

	["s1"] = true,
	["s2"] = true,
	["s3"] = true,
	["s4"] = true,
	["s5"] = true,
	["s6"] = true,
	["s7"] = true,
	["s8"] = true,
	["s9"] = true,
	["s10"] = true,
	["s11"] = true,
	["s12"] = true,
	["s13"] = true,
	["s14"] = true,
	["s15"] = true,
	["s16"] = true,
	["s17"] = true,
	["s18"] = true,
	["s19"] = true,
	["s20"] = true,
	["s21"] = true,
	["s22"] = true,
	["s23"] = true,
	["s24"] = true,
	["s25"] = true,
	["s26"] = true,
	["s27"] = true,
	["s28"] = true,
	["s29"] = true,
	["s30"] = true,

	["h1"] = true,
	["h2"] = true,
	["h3"] = true,
	["h4"] = true,
	["h5"] = true,
	["h6"] = true,
}
RUST.Config.Inventory.DelayUse = 2 // Secs delay when using a item

//Food system
RUST.Config.DefaultFood = 1080
RUST.Config.MaxFood = 3000
RUST.Config.FoodNutrition = {
	["cooked_chicken_breast"] = 500,
}