GM.Version = "1.0.0"
GM.Name = "Rust Legacy"
GM.Author = "William"
DeriveGamemode("sandbox")

local function LoadModules()
	local root = GM.FolderName.."/gamemode/modules/"

	local _, folders = file.Find(root.."*", "LUA")

	for _, folder in SortedPairs(folders, true) do
		for _, File in SortedPairs(file.Find(root .. folder .."/sh_*.lua", "LUA"), true) do
			include(root.. folder .. "/" ..File)
		end
		for _, File in SortedPairs(file.Find(root .. folder .."/cl_*.lua", "LUA"), true) do
			include(root.. folder .. "/" ..File)
		end
	end
end

RUST = {}

RUST.Config = RUST.Config or {}

include("config/config.lua")
LoadModules()

function GM:OnSpawnMenuOpen()
	if not LocalPlayer():IsSuperAdmin() then return end

	-- Let the gamemode decide whether we should open or not..
	if ( !hook.Call( "SpawnMenuOpen", GAMEMODE ) ) then return end

	if ( IsValid( g_SpawnMenu ) ) then
	
		g_SpawnMenu:Open()
		menubar.ParentTo( g_SpawnMenu )

	end
	
end