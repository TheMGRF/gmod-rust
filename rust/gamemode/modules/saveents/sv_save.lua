local entsToSave = {
	"metal_door",
	"metal_bed",
	"storage_small",
	"wood_ceiling",
	"wood_doorway",
	"wood_foundation",
	"wood_pillar",
	"wood_ramp",
	"wood_spikes",
	"wood_stairs",
	"wood_wall",
	"wood_window"
}

if RUST.Config.SleepersEnabled then
	table.insert(entsToSave, "sleeper")
end

function RustSaveEnts()
	print(GAMEMODE.Name..": Server is saving map.")
	local tbl = {}
	local num = 1
	for k,v in pairs(ents.GetAll()) do
		if table.HasValue(entsToSave, v:GetClass()) then
			table.insert(tbl, num)
			if v:GetClass() == "metal_bed" then
				tbl[num] = {
					["class"] = v:GetClass(),
	                ["pos"] = v:GetPos(),
	                ["ang"] = v:GetAngles(),
	                ["hp"] = v:Health(),
	                ["plyid"] = v:GetplyID()
	            }
			elseif v:GetClass() == "metal_door" then
				if v.OPos == nil then continue end
				tbl[num] = {
					["class"] = v:GetClass(),
	                ["pos"] = v.OPos,
	                ["ang"] = v.OAng,
	                ["hp"] = v:Health(),
	                ["code"] = v:GetCode(),
	                ["plyid"] = v:GetplyID()
	            }
			elseif v:GetClass() == "sleeper" then
				tbl[num] = {
					["class"] = v:GetClass(),
	                ["pos"] = v:GetPos(),
	                ["ang"] = v:GetAngles(),
	                ["hp"] = v:Health(),
	                ["plyname"] = v:GetplyName(),
	                ["plyid"] = v:GetplyID()
	            }
			else
				tbl[num] = {
					["class"] = v:GetClass(),
	                ["pos"] = v:GetPos(),
	                ["ang"] = v:GetAngles(),
	                ["hp"] = v:Health()
	            }
	        end
            num=num+1
		end
	end
	tbl = util.TableToJSON(tbl)
	file.Write("rust/spawnlist.txt", tbl)
	print(GAMEMODE.Name..": Saved all entities to the map!")
end

concommand.Add("rust_saveents", RustSaveEnts)

function RustSpawnEnts()
	print(GAMEMODE.Name..": Spawning all entities in the map!")
	if not file.Exists("rust/spawnlist.txt", "data") then print(GAMEMODE.Name..": Rust entities file doesn't exist. Creating file.") file.Write("rust/spawnlist.txt", "") else
		if file.Read("rust/spawnlist.txt", "data") == "" then print(GAMEMODE.Name..": Spawned all entities in the map!") return end
		tbl = util.JSONToTable(file.Read("rust/spawnlist.txt", "data"))
		for k,v in pairs(tbl) do
			local ent = ents.Create(v.class)
			ent:SetPos(v.pos)
			ent:SetAngles(v.ang)
			ent:Spawn()
			ent:SetHealth(v.hp)
			if v.class == "metal_bed" then
				ent:SetCooldown(CurTime())
				ent:SetplyID(v.plyid)
			end
			if v.class == "metal_door" then
				ent:SetCode(v.code)
				ent:SetplyID(v.plyid)
			end
			if v.class == "sleeper" then
				ent:SetplyName(v.plyname)
				ent:SetplyID(v.plyid)
			end
		end
	end
	print(GAMEMODE.Name..": Spawned all entities in the map!")
end

concommand.Add("rust_spawnents", RustSpawnEnts)
