local ply = FindMetaTable("Player")

function ply:IsInjured()
	if !IsValid(self) then error("Attemped to get if ent is injured but ent is nil/not valid.") return end
	return self:GetNWBool("Injured", false)
end