local ply = FindMetaTable("Player")

function ply:SetInjured(bool)
	if !isbool(bool) then error("Attempted to set injury to "..tostring(bool).." it has to be a boolean.") return end
	self:SetNWBool("Injured", bool)
end

function ply:UnInjure()
	if IsValid(self) && self:IsPlayer() then 
        self:SetJumpPower(200)
		self:SetViewOffset(Vector(0,0,64))
		self:SetViewOffsetDucked(Vector(0,0,26))
		self:SetRunSpeed(RUST.Config.RunSpeed)
		self:SetWalkSpeed(RUST.Config.WalkSpeed)
		self:SetInjured(false)
	else
		error("Attempted to uninjure a non player ent / ent is not valid or a player.")
	end
end

function ply:Injury(dmg)
	if dmg >= RUST.Config.InjuryMinDMG && !self:IsInjured() then
		self:SetViewOffset(Vector(0,0,15)) 
		self:SetViewOffsetDucked(Vector(0,0,15)) 
		self:SetJumpPower(1)
		self:SetRunSpeed(50)
		self:SetWalkSpeed(50)
		self:SetInjured(true)
		timer.Create("InjuryT"..self:SteamID(), RUST.Config.InjuryTime, 1, function()
			if IsValid(self) && self:IsPlayer() then 
	            self:UnInjure()
			end
		end)	
	end
end

function ply:RespawnedInjury()
	if self:IsInjured() then
		self:UnInjure()
		timer.Destroy("InjuryT"..self:SteamID())
	end
end