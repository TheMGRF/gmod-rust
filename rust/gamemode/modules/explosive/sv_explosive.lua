local ent = FindMetaTable("Entity")
function ent:TakeDMG(dmg, bool)
	if isbool(bool) && bool then
		if self:Health()-dmg:GetDamage() > 0 then
			self:SetHealth(self:Health()-dmg:GetDamage())
		else
			self:Remove()
			return true
		end
	else
		if not dmg:GetAttacker():IsPlayer() then
			if self:Health()-dmg:GetDamage() > 0 then
				self:SetHealth(self:Health()-dmg:GetDamage())
			else
				self:Remove()
				return true
			end
		end
	end
	return false
end