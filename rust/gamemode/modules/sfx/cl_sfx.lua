sound.Add({
	name = "day_wind_loop",
	channel = CHAN_AUTO,
	volume = 1.0,
	level = 75,
	pitch = 100,
	sound = "env/sfx/day_wind_loop.wav"
})

sound.Add({
	name = "crickets_loop",
	channel = CHAN_AUTO,
	volume = 1.0,
	level = 75,
	pitch = 100,
	sound = "env/sfx/crickets_loop.wav"
})

local function env_sound()
	if timer.Exists("day_wind_loop_timer") then timer.Destroy("day_wind_loop_timer") end
	LocalPlayer():StopSound("day_wind_loop")
	//LocalPlayer():EmitSound("day_wind_loop")
	timer.Create("day_wind_loop_timer", SoundDuration("day_wind_loop")-.1, 0, function()
		--print("next cycle env")
		//if time==day then
		//LocalPlayer():EmitSound("day_wind_loop")
	end)
end

hook.Add("InitPostEntity", "play_background_sounds", env_sound)

concommand.Add("Reset_ENV_Sound", env_sound)