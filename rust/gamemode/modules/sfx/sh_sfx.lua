hook.Add("EntityEmitSound", "rust:replace:sounds", function(s)
	if string.StartWith(s.SoundName, "weapons/fx/nearmiss") then return false end
	if RUST.Config.SFXReplace[s.SoundName] then
		s.SoundName = RUST.Config.SFXReplace[s.SoundName]
		--s.SoundLevel = 150
		--PrintTable(s)
		return true
	end
	//PrintTable(s)
end)

/*hook.Add("PlayerFootstep", "rust:replace:stepsound", function(ply, pos, foot, sound, volume, rf)
	if RUST.Config.SFXReplaceFootsteps[sound] then
		if SERVER then
			ply:EmitSound(RUST.Config.SFXReplaceFootsteps[sound])
		else
			sound.Play()
		end
		--PrintTable(s)
		return true
	end
end)*/

function HitObjImpactSnd(ent, vec)
	if vec == nil then vec = ent:GetPos() end
	local class = ent:GetClass()
	local sound = RUST.Config.EntityImpactSoundBullet[class]

	ent:EmitSound(sound)
end

sound.Add({
	name = "rust_door_opened",
	channel = CHAN_AUTO,
	volume = 1.0,
	level = 75,
	pitch = 100,
	sound = "prop/door/sfx/door_open.wav"
})

sound.Add({
	name = "rust_door_closed",
	channel = CHAN_AUTO,
	volume = 1.0,
	level = 75,
	pitch = 100,
	sound = "prop/door/sfx/door_close.wav"
})