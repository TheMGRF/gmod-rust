if !file.Exists("rust/player_data", "data") then print(GAMEMODE.Name..": Rust player_data directory in data doesn't exist creating dir.") file.CreateDir("rust/player_data") end

local function loadData(ply)
	if file.Exists("rust/player_data/"..ply:SteamID64(), "data") then
		LoadSleeperInfo(ply:SteamID64())
		ply:LoadInventory()
		ply:LoadFoodValue()
	else
		// First join
		ply:ResetInventory()
		ply:SetFood(RUST.Config.DefaultFood)
		file.CreateDir("rust/player_data/"..ply:SteamID64())
		ply:Hint("Welcome to Rust Legacy.")
	end
end
hook.Add("PlayerInitialSpawn", "Load_Player_Data", loadData)