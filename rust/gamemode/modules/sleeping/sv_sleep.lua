local PlayerData = {}
local plyMeta = FindMetaTable("Player")
local function SaveSleeperStats(SID64)
	local tbl = PlayerData[SID64]
	file.Write("rust/player_data/"..SID64.."/sleeper.txt", util.TableToJSON(tbl))
end

local function WritePlayerDataToFiles()
	for k,v in pairs(player.GetAll()) do
		SaveSleeperStats(v:SteamID64())
	end
end

function LoadSleeperInfo(SID64)
	if !file.Exists("rust/player_data/"..SID64.."/sleeper.txt", "data") then return true end
	local tbl = util.JSONToTable(file.Read("rust/player_data/"..SID64.."/sleeper.txt", "data"))
	PlayerData[SID64] = tbl
	return false
end

/*function LoadPlyPos()
	print(GAMEMODE.Name..": Loading sleepers pos.")
	if not file.Exists("rust/sleepers.txt", "data") then print(GAMEMODE.Name..": Rust sleepers file doesn't exist. Creating file.") file.Write("rust/sleepers.txt", "") else
		if file.Read("rust/sleepers.txt", "data") == "" then print(GAMEMODE.Name..": Loaded sleepers pos.") return end
		plyspawnPos = util.JSONToTable(file.Read("rust/sleepers.txt", "data"))
	end
	print(GAMEMODE.Name..": Loaded sleepers pos.")
end

function SaveplyPos()
	local tbl = util.TableToJSON(plyspawnPos)
	file.Write("rust/sleepers.txt", tbl)
end*/

local function PlayerSpawnedSleeper(ply)
	if !isbool(ply.initspawned) then
		if PlayerData[ply:SteamID64()] then
			if PlayerData[ply:SteamID64()].dead then
				ply:SetPos(Vector(0,0,0))
				ply:KillSilent()
				net.Start("PlayerDScr")
					local txt = PlayerData[ply:SteamID64()].dmsg
					net.WriteString(txt)
				net.Send(ply)
			else
				ply:SetPos(PlayerData[ply:SteamID64()].pos)
				ply:SetHealth(PlayerData[ply:SteamID64()].hp)
				for k,v in pairs(ents.FindByClass("sleeper")) do
					if v:GetplyID() != ply:SteamID64() then continue end
					v:Remove()
					return
				end
			end
		else
			ply:RandomPos()
			ply:SetHealth(RUST.Config.Health)
		end
		ply.initspawned = true
	end
end

hook.Add("PlayerSpawn", "PlayerReturnPos", PlayerSpawnedSleeper)

local function SavePlayerDataTable(ply, bool)
	bool = bool or false
	PlayerData[ply:SteamID64()] = {
		["hp"] = ply:Health(),
        ["pos"] = ply:GetPos(),
        ["dead"] = !ply:Alive(),
        ["dmsg"] = "You died while sleeping..."
    }
    if ply:Alive() && RUST.Config.SleepersEnabled && bool then
   		local ent = ents.Create("sleeper")
   		ent:SetPos(ply:GetPos())
   		ent:Spawn()
   		ent:SetplyID(ply:SteamID64())
   		ent:SetplyName(ply:GetName())
   		--ent:SetItems(ply:GetInvItems())
   	end
end

hook.Add("PlayerDisconnected", "PlayerDisPos", function(ply)
 	SavePlayerDataTable(ply)
	SaveSleeperStats(ply:SteamID64())
	PlayerData[ply:SteamID64()] = nil
end)

function ChangeSleeperStats(v1,v2,v3,v4,v5)
	if PlayerData[v1] == nil then
		if LoadSleeperInfo(v1) then
			--print("Loading offline player data: "..v1)
		end
	end
	if v4 == nil then v4 = PlayerData[v1].dead end
	PlayerData[v1] = {
		["hp"] = v2 or PlayerData[v1].hp,
        ["pos"] = v3 or PlayerData[v1].pos,
        ["dead"] = v4,
        ["dmsg"] = v5 or PlayerData[v1].dmsg
    }
    SaveSleeperStats(v1)
    PlayerData[v1] = nil
end

function plyMeta:PlayerRespawnedPos(pos)
	ChangeSleeperStats(self:SteamID64(),RUST.Config.Health,pos,false,"You died while sleeping...")
end

function SaveAllPlyPos(bool)
	bool = bool or false
	print(GAMEMODE.Name..": Saving all player's pos.")
	for k,v in pairs(player.GetAll()) do
	 	SavePlayerDataTable(v, bool)
	end
	WritePlayerDataToFiles()
	print(GAMEMODE.Name..": Saved all player's pos.")
end

concommand.Add("sleep_tbl", function()
	PrintTable(PlayerData)
end)