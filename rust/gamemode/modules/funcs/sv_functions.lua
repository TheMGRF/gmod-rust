local ply = FindMetaTable("Player")

function RandomVecInWorld()
    local vec = RandomVector(Vector(-10000,-10000,-1), Vector(10000,10000,10000))
	if util.IsInWorld(vec) then
		local tr = util.TraceHull( {
			start = vec,
			endpos = vec-Vector(0,0,1000),
		})
		if tr.Entity && IsEntity(tr.Entity) && tr.Entity:IsWorld() then
			return tr.HitPos
		else
			return RandomVecInWorld()
		end
	else
		return RandomVecInWorld()
	end
end

function ply:RandomPos()
	local vec = RandomVecInWorld()
	self:SetPos(vec)
	self:PlayerRespawnedPos(vec)
end