local plyMeta = FindMetaTable("Player")
local entMeta = FindMetaTable("Entity")

function table.HasKey(table, key)
	for k, v in pairs(table) do
		if k == key then
			return true
		end
	end
	return false
end

function plyMeta:Hint(text, time)
	time = time or 5
	if SERVER then
		self:SendLua([[notification.AddLegacy("]]..text..[[", NOTIFY_GENERIC, ]]..tostring(time)..[[)]])
		self:SendLua([[print("NOTIFICATION: ]]..text..[[")]])
	else
		notification.AddLegacy(text, NOTIFY_GENERIC, tostring(time))
		print('NOTIFICATION: '..text)
	end
end

function RandomVector(min, max)
    return Vector(math.random(min.x, max.x), math.random(min.y, max.y), math.random(min.z, max.z))
end