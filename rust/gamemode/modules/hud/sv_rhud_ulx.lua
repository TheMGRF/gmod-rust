util.AddNetworkString( "RHUD_CLOAK" )

if RUST.Config.playerInfo then
	hook.Add( "ULibPostTranslatedCommand", "RHUD_CLOAK_REMOVEHUD", function( ply, cmd, args )
		if cmd == "ulx cloak" then
			net.Start( "RHUD_CLOAK" )
			net.WriteBool( true )
			net.WriteTable( args[2] )
			net.Broadcast()
		elseif cmd == "ulx uncloak" then
			net.Start( "RHUD_CLOAK" )
			net.WriteBool( false )
			net.WriteTable( args[2] )
			net.Broadcast()
		end
	end )
end