surface.CreateFont("InstructionalInfo", {
    font = "Minecraftia 2.0",
    size = 22,
})

local hit_material = Material("materials/rust/hitnotification.png")
local hit_x, hit_y = (ScrW()/2)-16, (ScrH()/2)-16
hook.Add("HUDPaint", "HealthHUD", function()
	if not GetConVar("cl_drawhud") then return end
	if GetConVarNumber("cl_drawhud") == 0 then return end
	local ply = LocalPlayer()
	//HITMARKER START//
	--print(ply.hitmarker)
	if isnumber(ply.hitmarker) && ply.hitmarker >= CurTime() then
		local a = 255*((ply.hitmarker-CurTime())/RUST.Config.HitmarkerTime)
		a = math.Clamp(a, 0, 255)
		surface.SetDrawColor(255,255,255, a)
		surface.SetMaterial(hit_material)
		surface.DrawTexturedRect(hit_x, hit_y, 32, 32)
	end
	//HITMARKER END//
	//START PROP HEALTH DISPLAY//
	local tr = ply:GetEyeTrace()
	if tr.Entity && IsValid(tr.Entity) && (string.StartWith(tr.Entity:GetClass(), "wood_") or tr.Entity:GetClass()=="metal_door") && (ply:GetPos():Distance(tr.Entity:GetPos()) < 250) then
		local screen = (tr.Entity:GetPos()+tr.Entity:OBBCenter()):ToScreen()
		if screen.visible then
			if lastProp != tr.Entity then
				smoothPropHealth = tr.Entity:Health()
				lastProp = tr.Entity
			end

			draw.DrawText(tr.Entity:Health().."/"..tr.Entity:GetMaxHealth(), "Trebuchet24", screen.x, screen.y, Color(255, 255, 255), TEXT_ALIGN_CENTER)
			draw.RoundedBox(2, screen.x-50, screen.y-2.5, 100, 5, Color(255, 255, 255))
			smoothPropHealth = math.Approach(smoothPropHealth, tr.Entity:Health(), 50 * FrameTime())
			if tr.Entity:Health() != 0 then
				draw.RoundedBox(2, screen.x-49, screen.y-1.5, 98*(smoothPropHealth/tr.Entity:GetMaxHealth()), 3, Color(25, 255, 25))
			end
		end
	end
	//END PROP HEALTH DISPLAY//
	//START ITEMSACK DISPLAY//
	local ent = ply:GetEyeTrace().Entity
	if IsValid(ent) && ent:GetClass() == "dropped_item" && (ent:GetPos():Distance(LocalPlayer():GetPos()) < 100) then
		local ts = ent:GetPos():ToScreen()
		local s = ""
		if ent:GetItmAmt() > 1 then
			s = " x"..ent:GetItmAmt()
		end
		draw.SimpleTextOutlined("Take ´"..RUST.Inventory.Item[ent:GetItmName()].name..s.."´", "InstructionalInfo", ts.x, ts.y-50, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP, 2, Color(0,0,0, 100))
	end
	//END ITEMSACK DISPLAY//
end)