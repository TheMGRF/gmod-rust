local theme = RUST.Config.colors

surface.CreateFont( "rhud_Regular", {
    font = theme.font_regular,
    size = 11,
    antialias = false,
})

surface.CreateFont( "rhud_Overhead", {
    font = theme.font_overhead,
    size = 24,
})