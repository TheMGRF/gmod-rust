local cfg = RUST.Config
local colors = cfg.colors
local txt = cfg.text

local hideTable = {
		[ "CHudHealth" ] = true,
		[ "CHudBattery" ] = true,
		[ "CHudAmmo" ] = true,
		[ "CHudSecondaryAmmo" ] = true,
}

hook.Add( "HUDShouldDraw", "RHUD_HIDE_DEFAULT_HUD", function( element )
	if ( hideTable[ element ] ) then return false end
end )

local radsAmount = 0
local healthBar = 0
local w, h = 108, 53 // 110, 56 old size
local PosX = ScrW() - w - 17
local PosY = ScrH() - h -9

local dontDraw = {}
net.Receive( "RHUD_CLOAK", function()
	local cloakState = net.ReadBool()
	local cloakTable = net.ReadTable()
	for _, v in pairs( cloakTable ) do
		if cloakState == true then
			dontDraw[v] = true
		elseif cloakState == false then
			dontDraw[v] = nil
		end
	end
end )
hook.Add( "HUDPaint", "RHUD_DRAW_HUD", function()
	if not GetConVar("cl_drawhud") then return end
	if GetConVarNumber("cl_drawhud") == 0 then return end
	local ply = LocalPlayer()
	-- Base
	surface.SetDrawColor( colors.Shadow )
	for i=1,5 do
		surface.DrawRect( PosX + i, PosY + i, w+i, h+i )
	end
	surface.SetDrawColor( colors.MainColor )
	surface.DrawRect( PosX, PosY, 108, 53 )
	surface.SetDrawColor( colors.Black )
	surface.DrawRect( PosX + 47, PosY + 7, 54, 28 )

	-- Health
	draw.SimpleText( txt.health, "rhud_Regular", PosX + 5, PosY + 9, colors.White, 0, 3)
	healthBar = math.Clamp(ply:Health(), 0, 100)
	surface.SetDrawColor( colors.HealthColor )
	surface.DrawRect( PosX + 49, PosY + 9, healthBar / 2, 10 )
	draw.SimpleTextOutlined( ply:Health(), "rhud_Regular", PosX + 74, PosY + 8, colors.White, 1, 2, 1, colors.outline)

	-- Food
	local foodAmount = ply:GetFood()
	draw.SimpleText( txt.food, "rhud_Regular", PosX + 5, PosY + 22, colors.White, 0, 3)
	surface.SetDrawColor( colors.FoodColor )
	if ply:GetFood() > 0 then 
		surface.DrawRect( PosX + 49, PosY + 23, 50 * (ply:GetFood()/RUST.Config.MaxFood), 10 )
	end
	draw.SimpleTextOutlined( string.Comma( foodAmount ), "rhud_Regular", PosX + 74, PosY + 23, colors.White, 1, 2, 1, colors.outline)

	-- Radiation
	draw.SimpleText( txt.rads, "rhud_Regular", PosX + 5, PosY + 35, colors.White, 0, 3)
	draw.SimpleTextOutlined( string.Comma( radsAmount ), "rhud_Regular", PosX + 75, PosY + 35, colors.White, 1, 2, 1, colors.outline)

	-- Overhead HUD
	if cfg.playerInfo then
		local tracedEntity = ply:GetEyeTrace().Entity
		if tracedEntity:IsPlayer() and ply:GetEyeTrace().Fraction < 0.005 then
			local pl = tracedEntity
			local pPos = pl:EyePos()
			pPos = pPos:ToScreen()
			if dontDraw[pl]==nil then
				draw.SimpleText( tracedEntity:Nick(), "rhud_Overhead", pPos.x, pPos.y - 64, Color(255,255,255, 100), 1, 1 )
			end		
		end
	end
end)

hook.Add("HUDDrawTargetID", "RHUD_DISABLE_TARGETID", function()
	return false
end )