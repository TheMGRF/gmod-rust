local function IsInRoom(listener, talker) -- IsInRoom function to see if the player is in the same room.
	local tracedata = {}
	tracedata.start = talker:GetShootPos()
	tracedata.endpos = listener:GetShootPos()
	local trace = util.TraceLine(tracedata)

	return not trace.HitWorld
end

function GM:PlayerCanHearPlayersVoice(listener, talker)
	if listener:GetPos():Distance( talker:GetPos() ) > 500 then return false end
	return true, true
end