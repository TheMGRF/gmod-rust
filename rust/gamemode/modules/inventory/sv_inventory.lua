local plyMeta = FindMetaTable("Player")
util.AddNetworkString("SendInvInfo")
util.AddNetworkString("inv_moveitm")
util.AddNetworkString("inv_splititem")
util.AddNetworkString("inv_useitem")
util.AddNetworkString("inv_equipitem") // hotbar
util.AddNetworkString("inv_unload")

net.Receive("SendInvInfo", function(len, ply)
	if ply.requested then return end
	ply.requested = true
	ply:SendInvInfo()
end)

function plyMeta:SendInvInfo(tbl)
	if tbl == nil then tbl = self.Inventory end
	local compressed = util.TableToJSON(tbl) -- util.compress is broken
	net.Start("SendInvInfo")
		net.WriteString(compressed)
	net.Send(self)
end

function plyMeta:SaveInventory(b)
	if b == nil then b = true end

	file.Write("rust/player_data/"..self:SteamID64().."/inventory.txt", util.TableToJSON(self.Inventory))
	if b then
		self:SendInvInfo()
	end
end

concommand.Add("updateInv", function(ply) ply:SendInvInfo() end)

concommand.Add("AddInv", function(ply) ply:GiveItem("wood_planks", 2) end)

concommand.Add("ResetInv", function(ply) ply:ResetInventory() end)

concommand.Add("HasItm", function(ply) ply:HasItem("wood_planks", 22) end)

concommand.Add("TakeItm", function(ply) ply:TakeItem("wood_planks", 8) end)

function plyMeta:LoadInventory()
	if file.Exists("rust/player_data/"..self:SteamID64().."/inventory.txt", "data") then
		local tbl = util.JSONToTable(file.Read("rust/player_data/"..self:SteamID64().."/inventory.txt", "data"))
		self.Inventory = tbl
		--timer.Simple(1, function() self:SendInvInfo(tbl) end) client asks for info
	else
		self:ResetInventory()
	end
end

function plyMeta:ResetInventory()
	self.Inventory = {}
	local s = 1
	for k,v in pairs(RUST.Config.Inventory.DefaultItems) do
		self:GiveItemNS(k, v, nil, false, true)
	end
	self:SaveInventory()
end

function plyMeta:GiveItemNS(itm, amt, extra, si, b)
	if RUST.Inventory.Item[itm] == nil then print("Invalid item given: "..itm) return end
	if RUST.Inventory.Item[itm].max < amt then
		self:GiveItemNS(itm, amt-RUST.Inventory.Item[itm].max, extra, si, b)
		amt = RUST.Inventory.Item[itm].max
	end
	local slot = self:FindEmptySlot(b)
	if slot == nil then
		local sack = ents.Create("dropped_item")
		sack:SetPos(self:GetPos())
		sack:SetItmName(itm)
		sack:SetItmAmt(amt)
		if istable(extra) then
			sack:Setextra(util.TableToJSON(extra))
		end
		sack:Activate()
		sack:Spawn()
		return 
	end
	self.Inventory[slot] = {
		itm,
		amt,
	}
	if istable(extra) then
		self.Inventory[slot]["extra"] = extra
	end
	if si == nil or si == true then
		self:SaveInventory()
	end
end

function plyMeta:GiveItem(itm, amt, extra)
	if RUST.Inventory.Item[itm] == nil then print("Invalid item given: "..itm) return end
	local inv = self.Inventory
	local d = true
	local max = RUST.Inventory.Item[itm].max
	for i=1,30 do
		if inv["s"..i] == nil then continue end
		if inv["s"..i][1] != itm then continue end
		if inv["s"..i][2] >= max then continue end
		if (inv["s"..i][2] + amt) > max then
			local lamt = (inv["s"..i][2] + amt) - max
			inv["s"..i][2] = max
			self:GiveItem(itm, lamt, extra)
		else
			inv["s"..i][2] = inv["s"..i][2] + amt
		end
		d = false
		break
	end
	if d then
		self:GiveItemNS(itm, amt, extra)
		return
	end
	self:SaveInventory()
end

function plyMeta:TakeItem(itm, amt)
	if RUST.Inventory.Item[itm] == nil then print("Invalid item given: "..itm) return end
	local inv = self.Inventory
	local lamt = amt // Left to take amount
	for k,v in pairs(inv) do
		if v[1] != itm then continue end
		if lamt <= 0 then break end
		--print("lamt: ".. lamt)
		local tamt = v[2] // Saving before it gets deleted
		if v[2] > lamt then
			--print("gat: "..v[2].." "..v[2]-lamt)
			inv[k][2] = v[2]-lamt
		else
			--print("gay: "..v[2].." "..v[2]-lamt)
			inv[k] = nil
			self:UnEquipItm(inv[k])
		end
		lamt = lamt-tamt
	end
	self:SaveInventory()
end

net.Receive("inv_moveitm", function(len, ply)
	local s1, s2 = net.ReadString(), net.ReadString()
	if !RUST.Config.Inventory.Slots[s2] then return end
	if s1==s2 then return end
	local inv = ply.Inventory
	if inv[s1] == nil then return end

	if inv[s2] == nil then
		inv[s2] = inv[s1]
	elseif inv[s1][1] == inv[s2][1] then
		local max = RUST.Inventory.Item[inv[s1][1]].max
		if inv[s2][2] == max then return end
		if (inv[s2][2] + inv[s1][2]) > max then
			--print("s1: "..inv[s1][2], "s2: "..inv[s2][2], "div: "..((inv[s2][2] + inv[s1][2]) - max))
			local lamt = (inv[s2][2] + inv[s1][2]) - max
			inv[s2][2] = max // Dont put infront of give item, will fuck it up
			ply:GiveItem(inv[s1][1], lamt)
		else
			inv[s2][2] = inv[s2][2] + inv[s1][2]
		end
	else
		return
	end
	ply:UnEquipItm(s1)
	inv[s1] = nil
	ply:SaveInventory()
end)

net.Receive("inv_splititem", function(len, ply)
	local slot = net.ReadString()
	if !RUST.Config.Inventory.Slots[slot] then return end
	local inv = ply.Inventory
	if inv[slot] == nil then return end
	if ply:FindEmptySlot() == nil then return end // There isnt a extra slot available
	local itm, amt = inv[slot][1], inv[slot][2]/2
	inv[slot][2] = math.Round(amt)
	ply:GiveItemNS(itm, math.floor(amt))
	ply:SaveInventory()
end)

net.Receive("inv_useitem", function(len, ply)
	local slot = net.ReadString()
	ply:UseItem(slot)
end)

function plyMeta:UseItem(slot)
	if !RUST.Config.Inventory.Slots[slot] then return end
	if self.LastUsed && self.LastUsed >= CurTime() then return end
	local inv = self.Inventory
	if inv[slot] == nil then return end
	if !isfunction(RUST.Inventory.Item[inv[slot][1]].use) then print("Inventory use: "..inv[slot][1].." is not a usable item, ply: "..self:Nick()) return end
	inv[slot][2] = inv[slot][2]-1
	RUST.Inventory.Item[inv[slot][1]].use(self, slot)
	if inv[slot][2] <= 0 then
		inv[slot] = nil
	end
	self:SaveInventory()
	self.LastUsed = CurTime()+RUST.Config.Inventory.DelayUse
end

function plyMeta:UnEquipItm(slot)
	if self.equippedslot == nil then return end
	if slot == nil then slot = self.equippedslot end
	local inv = self.Inventory
	if self.equippedslot == slot then
		if RUST.Inventory.Item[inv[self.equippedslot][1]].gun then
			self:StripWeapon(inv[self.equippedslot][1])
			self.equippedslot = nil
		end
	end
end

function plyMeta:EquipItm(slot)
	local inv = self.Inventory
	if !RUST.Inventory.Item[inv[slot][1]].gun && !RUST.Inventory.Item[inv[slot][1]].use then return end
	if self.equippedslot != nil && RUST.Inventory.Item[inv[slot][1]].gun then self:UnEquipItm() end
	if RUST.Inventory.Item[inv[slot][1]].gun then
		self:Give(inv[slot][1])
		self:SelectWeapon(inv[slot][1])
		net.Start("inv_equipitem")
		net.Send(self)
		self.equippedslot = slot
	else
		if RUST.Inventory.Item[inv[slot][1]].use then
			self:UseItem(slot)
		end
	end
end

net.Receive("inv_equipitem", function(len, ply)
	local slot = "h"..net.ReadUInt(3)
	local inv = ply.Inventory
	if ply.equippedslot == nil && inv[slot] == nil then return end
	if ply.equippedslot != nil && (ply.equippedslot == slot or inv[slot] == nil) then
		ply:UnEquipItm()
		return
	end
	if ply.equippedslot != slot then
		ply:EquipItm(slot)
	end
end)

hook.Add("PlayerDeath", "inv_reseteslot", function(ply)
	ply.equippedslot = nil
end)