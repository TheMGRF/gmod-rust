RUST.Inventory.Item = {}

local i = "wood_planks"
RUST.Inventory.Item[i] = {}
RUST.Inventory.Item[i].name = "Wood Planks"
RUST.Inventory.Item[i].icon = "/tex/"..i.."_icon.png"
RUST.Inventory.Item[i].max = 10

local i = "cooked_chicken_breast"
RUST.Inventory.Item[i] = {}
RUST.Inventory.Item[i].name = "Cooked Chicken Breast"
RUST.Inventory.Item[i].icon = "/tex/"..i.."_icon.png"
RUST.Inventory.Item[i].max = 3
RUST.Inventory.Item[i].use = function(ply, slot) ply:AddFood(RUST.Config.FoodNutrition[i]) ply:EmitSound("item/sfx/eating.wav") end
RUST.Inventory.Item[i].food = true // will make option displayed as "Consume"

local i = "rust_m4"
RUST.Inventory.Item[i] = {}
RUST.Inventory.Item[i].name = "M4"
RUST.Inventory.Item[i].icon = "/tex/m4_icon.png"
RUST.Inventory.Item[i].max = 1
RUST.Inventory.Item[i].gun = true

local i = "building_plan"
RUST.Inventory.Item[i] = {}
RUST.Inventory.Item[i].name = "#building_plan"
RUST.Inventory.Item[i].icon = "/tex/BlueprintIcon.png"
RUST.Inventory.Item[i].max = 1
RUST.Inventory.Item[i].gun = true

for k,v in pairs(RUST.Inventory.Item) do
	RUST.Inventory.Item[k].name = string.Replace(v.name, " ", "  ")
	RUST.Inventory.Item[k].mat = Material(v.icon)
end