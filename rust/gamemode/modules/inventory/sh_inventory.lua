local plyMeta = FindMetaTable("Player")

function plyMeta:GetInventory()
	return self.Inventory
end

function plyMeta:FindEmptySlot(b)
	if b then
		for i=1,6 do
			if self.Inventory["h"..i] == nil then
				return "h"..i
			end
		end
	end
	for i=1,30 do
		if self.Inventory["s"..i] == nil then
			return "s"..i
		end
	end
	if b then return end // stops it from doing the loop again
	for i=1,6 do
		if self.Inventory["h"..i] == nil then
			return "h"..i
		end
	end
end

function plyMeta:HasItem(itm, amt)
	if RUST.Inventory.Item[itm] == nil then print("Invalid item given: "..itm) return end
	local inv = self:GetInventory()
	local hamt = 0 // Has amount
	for k,v in pairs(inv) do
		if v[1] != itm then continue end
		hamt = hamt+v[2]
	end
	return (hamt >= amt)
end

if CLIENT then
	net.Receive("inv_equipitem", function()
		timer.Simple(.1, function() 
			local ply = LocalPlayer()
			local wep = ply:GetActiveWeapon()
			wep:Deploy()
		end)
	end)
end