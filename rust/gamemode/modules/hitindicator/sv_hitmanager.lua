util.AddNetworkString("hit_headshot")
util.AddNetworkString("hit_generic_marker")

hook.Add("PlayerShouldTakeDamage", "hitmarker_fx", function(ply, attacker)
	if ply == attacker then return end
	if attacker:IsPlayer() then
		if ply:LastHitGroup() == HITGROUP_HEAD then
			net.Start("hit_headshot")
			net.Send(attacker)
			ply:EmitSound("effect/sfx/impact/headshot-1.wav")
		end
		net.Start("hit_generic_marker")
		net.Send(attacker)
	end
end)
/*
concommand.Add("bot_invite", function()
	local lb = nil
	for i=1,50 do

		timer.Simple(i*.1, function()
			RunConsoleCommand("bot") 
			for k,v in pairs(player.GetAll()) do
				if !v:IsBot() then continue end
				lb = v
			end
			lb:Kill()
		end)
	end
end)*/