net.Receive("hit_headshot", function()
	surface.PlaySound("effect/sfx/impact/headshot-1.wav")
end)

net.Receive("hit_generic_marker", function()
	LocalPlayer().hitmarker = CurTime()+RUST.Config.HitmarkerTime
	surface.PlaySound("shared/sfx/hitnotification.wav")
end)