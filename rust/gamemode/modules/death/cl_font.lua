surface.CreateFont("YOU_ARE", {
	font = "Roboto Condensed Bold",
	size = 96,
})

surface.CreateFont("DEAD", {
	font = "Roboto Condensed Bold",
	size = 256,
	weight = 750
})

surface.CreateFont("Respawn", {
	font = "Roboto Condensed Bold",
	size = 86,
	weight = 610
})

surface.CreateFont("At_Camp", {
	font = "Roboto Condensed Bold",
	size = 42,
	weight = 1000
})

surface.CreateFont("what_happened", {
	font = "Roboto Condensed Regular",
	size = 40, // or 16
})