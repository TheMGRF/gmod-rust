local dead = Material("materials/rust/dead.png")
local backgroundcol = 32 // normally 26

local function deathScreen()
	surface.PlaySound("item/sfx/zipper.wav")

	local wepTxt = net.ReadString()

	local frame = vgui.Create("DFrame")
	frame:SetPos(0,0)
	frame:SetSize(ScrW(), ScrH())
	frame:MakePopup()
	frame:SetDraggable(false)
	frame:ShowCloseButton(false)
	frame:SetTitle("")
	function frame:Paint(w,h)
		draw.RoundedBox(0,0,0,w,h,Color(backgroundcol,backgroundcol,backgroundcol))
		surface.SetDrawColor(255,255,255)
		surface.SetMaterial(dead)
		surface.DrawTexturedRect((ScrW()/2)-(534/2), 115, 534, 272)
	end

	/*local yad = vgui.Create("DLabel", frame)
	local txt = "YOU ARE"
	local font = "YOU_ARE"
	yad:SetText(txt)
	yad:SetColor(Color(255,0,0))
	yad:SetFont(font)
	surface.SetFont(font)
	local w,h = surface.GetTextSize(txt)
	yad:SetSize(w,h)
	yad:SetPos((ScrW()/2)-(w/2), 150)

	local yad2 = vgui.Create("DLabel", frame)
	local txt = "DEAD"
	local font = "DEAD"
	yad2:SetText(txt)
	yad2:SetColor(Color(255,0,0))
	yad2:SetFont(font)
	surface.SetFont(font)
	local w,h = surface.GetTextSize(txt)
	yad2:SetSize(w,h)
	local x,y = yad:GetPos()
	yad2:SetPos((ScrW()/2)-(w/2), (y+yad:GetTall()+10)-h/2.5)*/

	local respawn = vgui.Create("DButton", frame)
	local txt = "RESPAWN"
	local font = "Respawn"
	respawn:SetText(txt)
	respawn:SetFont(font)
	surface.SetFont(font)
	local w,h = surface.GetTextSize(txt)
	respawn:SetSize(w,h)
	respawn:SetPos(ScrW()-(116+w), ScrH()-130-h+16) // 16 is some gay offset
	function respawn:DoClick()
		net.Start("RespawnPressed")
			net.WriteBool(false)
		net.SendToServer()
		frame:Close()
	end
	function respawn:Paint(w,h)
		if self:IsHovered() then
			self:SetColor(Color(255,255,255))
		else
			self:SetColor(Color(158,158,158))
		end
		draw.RoundedBox(0,0,0,w,h,Color(backgroundcol,backgroundcol,backgroundcol))
	end

	local respawnc = vgui.Create("DButton", frame)
	local txt = "AT A CAMP"
	local font = "At_Camp"
	respawnc:SetText(txt)
	respawnc:SetFont(font)
	surface.SetFont(font)
	local w,h = surface.GetTextSize(txt)
	respawnc:SetSize(w,h)
	--respawnc:SetPos(ScrW()-(25+w), ScrH()-75)
	local x,y = respawn:GetPos()
	respawnc:SetPos(ScrW()-(116+w), (y+respawn:GetTall()+22)-(h-2))
	function respawnc:DoClick()
		net.Start("RespawnPressed")
			net.WriteBool(true)
		net.SendToServer()
		frame:Close()
	end
	function respawnc:Paint(w,h)
		if self:IsHovered() then
			self:SetColor(Color(255,255,255))
		else
			self:SetColor(Color(158,158,158))
		end
		draw.RoundedBox(0,0,0,w,h,Color(backgroundcol,backgroundcol,backgroundcol))
	end

	local dr = vgui.Create("DLabel", frame)

	local wep = string.match(wepTxt, "___(.*)___")
	local wepName = "unknown"
	for k,v in pairs(list.Get("Weapon")) do
		if (v["ClassName"] == wep) then
			wepName = v["PrintName"]
		end
	end

	if (wep && wep != "") then
		wepTxt = string.Replace(wepTxt, "___"..wep.."___", wepName)
	end

	local font = "what_happened"
	dr:SetText(wepTxt)
	dr:SetFont(font)
	surface.SetFont(font)
	local w,h = surface.GetTextSize(wepTxt)
	dr:SetSize(w,h)
	dr:CenterHorizontal()
	dr:CenterVertical(0.55)
end

net.Receive("PlayerDScr", deathScreen)