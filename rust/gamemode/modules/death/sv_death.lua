util.AddNetworkString("PlayerDScr")
util.AddNetworkString("RespawnPressed")

local hitGroupValues = RUST.Config.HitGroups

local function deathcause(v,a)
	if v==a then
		return "You killed yourself. You silly sod."
	end
	if a:IsPlayer() then
		local hitGroup = v:LastHitGroup()
		local hitGroupName = "Unknown"
		if hitGroupValues[hitGroup] then
			hitGroupName = hitGroupValues[hitGroup]
		end

		local wep = a:GetActiveWeapon()
		if IsValid(wep) then
			wep = wep:GetClass()
		else
			wep = "unknown"
		end

		return a:GetName().." killed you using a ___"..wep.."___ with a hit to your "..hitGroupName.."."
	end
	if IsEntity(a) then
		return a:GetClass().." killed you."
	end
	return "You died somehow..."
end

hook.Add("DoPlayerDeath", "NotifyKiller", function(ply, attacker, dmg)
	if IsValid(attacker) && attacker:IsPlayer() and ply != attacker then
		attacker:Hint("You killed "..ply:GetName().."!")
	end
	if IsValid(ply) && ply:IsPlayer() then
		net.Start("PlayerDScr")
			local txt = deathcause(ply,attacker)
			net.WriteString(txt)
		net.Send(ply)
	end
end)

/*hook.Add("PlayerSilentDeath", "SilentDeSCR", function(ply)
	if IsValid(ply) && ply:IsPlayer() then
		net.Start("PlayerDScr")
			local txt = "You died somehow..."
			net.WriteString(txt)
		net.Send(ply)
	end
end)*/

net.Receive("RespawnPressed", function(len, ply)
	if net.ReadBool() then
		ply:Spawn()
		if IsValid(ply.bag) then
			if (ply.bag:GetCooldown()-CurTime()) < 1 then
				ply:Hint("You will not be able to spawn near here for 4 Minutes!")
				local pos = ply.bag:GetPos()+Vector(0,0,15)
				ply:SetPos(pos)
				ply:PlayerRespawnedPos(pos)
				ply.bag:SetCooldown(CurTime()+RUST.Config.SleepCooldown)
			else
				ply:Hint("You still have to wait to spawn in your sleeping bag!")
				ply:RandomPos()
			end
		else
			ply:Hint("You don't have a current sleeping bag!")
			ply:RandomPos()
		end
	else
		ply:Spawn()
		ply:RandomPos()
	end
	ply:SetHealth(RUST.Config.Health)
end)

local ply = FindMetaTable("Player")

function ply:SetBag()
	for k,v in pairs(ents.FindByClass("metal_bed")) do
		if v:GetplyID() == self:SteamID() then
			self.bag = v
			return
		end
	end
end