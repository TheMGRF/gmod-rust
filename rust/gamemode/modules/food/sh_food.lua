local plyMeta = FindMetaTable("Player")

function plyMeta:GetFood()
	return self:GetNWInt("rust_food", RUST.Config.DefaultFood)
end