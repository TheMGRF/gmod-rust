local plyMeta = FindMetaTable("Player")

function plyMeta:SetFood(int)
	if int > RUST.Config.MaxFood then
		int = RUST.Config.MaxFood
	end
	file.Write("rust/player_data/"..self:SteamID64().."/food.txt", tostring(int))
	self:SetNWInt("rust_food", int)
end

function plyMeta:AddFood(int)
	self:SetFood(self:GetFood()+int)
end

function plyMeta:LoadFoodValue()
	if !file.Exists("rust/player_data/"..self:SteamID64().."/food.txt", "data") then return true end
	local int = tonumber(file.Read("rust/player_data/"..self:SteamID64().."/food.txt", "data"))
	self:SetNWInt("rust_food", int)
	return false
end

hook.Add("PlayerSpawn", "Food_Reset", function(ply)
	if ply.initspawned then
		print("lol")
		ply:SetFood(RUST.Config.DefaultFood)
	end
end)