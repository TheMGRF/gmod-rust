GM.Version = "1.0.0"
GM.Name = "Rust Legacy"
GM.Author = "William"
DeriveGamemode("sandbox")

AddCSLuaFile("config/config.lua")
AddCSLuaFile("cl_init.lua")

RUST = {}

RUST.Config = RUST.Config or {}

include("config/config.lua")

local fol = GM.FolderName.."/gamemode/modules/"
local files, folders = file.Find(fol .. "*", "LUA")

for _, folder in SortedPairs(folders, true) do
	for _, File in SortedPairs(file.Find(fol .. folder .."/sh_*.lua", "LUA"), true) do
		AddCSLuaFile(fol..folder .. "/" ..File)
		include(fol.. folder .. "/" ..File)
	end

	for _, File in SortedPairs(file.Find(fol .. folder .."/sv_*.lua", "LUA"), true) do
		include(fol.. folder .. "/" ..File)
	end

	for _, File in SortedPairs(file.Find(fol .. folder .."/cl_*.lua", "LUA"), true) do
		AddCSLuaFile(fol.. folder .. "/" ..File)
	end
end

hook.Add("InitPostEntity", "RUST_SPAWNENTS", function()
	if !file.Exists("rust", "data") then print(GAMEMODE.Name..": Rust directory in data doesn't exist creating dir.") file.CreateDir("rust") end
	RustSpawnEnts()
	timer.Create("RUST_SaveStuff", 60, 0, function() RustSaveEnts() SaveAllPlyPos() end)
end)

hook.Add("ShutDown", "RUST_SAVEENTS", function()
	SaveAllPlyPos(true)
	RustSaveEnts()
end)

function GM:PlayerInitialSpawn( ply )
	ply:SetModel(RUST.Config.Model)
	ply:SetBag()
end

function GM:PlayerSpawn( ply )
	--ply:SetHealth(RUST.Config.Health)
	ply:SetupHands()
	ply:AllowFlashlight(false)
	ply:SetWalkSpeed(RUST.Config.WalkSpeed)
	ply:SetRunSpeed(RUST.Config.RunSpeed)
	ply:RespawnedInjury()
end

function GM:PlayerSetHandsModel( ply, ent )
	local simplemodel = player_manager.TranslateToPlayerModelName( ply:GetModel() )
	local info = player_manager.TranslatePlayerHands( simplemodel )
	if ( info ) then
		ent:SetModel( info.model )
		ent:SetSkin( info.skin )
		ent:SetBodyGroups( info.body )
	end
end

function GM:CanPlayerSuicide(ply)
	return true
end

function GM:GetFallDamage(ply, speed)
	if RUST.Config.FallDamage then
		ply:Injury((speed/20))
		return (speed/20)
	else
		return false
	end
end

function GM:CanDrive( ply, ent )
	if !ply:IsAdmin() then return false end
end

function GM:PlayerSpawnSWEP(ply, class)
	return ply:IsAdmin()
end

function GM:PlayerSpawnedSENT(ply, ent)
	if ent:GetClass() == "metal_bed" then
		ent:SetplyID(ply:SteamID())
		ply.bag = ent
	end
	if ent:GetClass() == "metal_door" then
		ent:SetplyID(ply:SteamID())
	end
	if ent:GetClass() == "sleeper" then
		ent:SetplyID(ply:SteamID())
		ent:SetplyName(ply:GetName())
	end
end