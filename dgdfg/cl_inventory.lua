surface.CreateFont("inventoryfont", {
	font = "Minecraftia 2.0",
	size = 11,
	antialias = false,
})

surface.CreateFont("inventoryfont_italic", {
	font = "Minecraftia 2.0",
	size = 11,
	italic = true,
	antialias = false,
})

net.Receive("SendInvInfo", function()
	local tbl = util.JSONToTable(net.ReadString()) -- util.compress is broken
	LocalPlayer().Inventory = tbl
end)

local function requestInv()
	net.Start("SendInvInfo")
	net.SendToServer()
end

hook.Add("InitPostEntity", "Rust_Inv", requestInv)

local function moveItem(s1, s2)
	if !RUST.Config.Inventory.Slots[s2] then return end
	if s1==s2 then return end
	local inv = LocalPlayer():GetInventory()
	if inv[s1] == nil then return end
	net.Start("inv_moveitm")
		net.WriteString(s1)
		net.WriteString(s2)
	net.SendToServer()

	LocalPlayer().Inventory = inv
end

local function rightClick(self, int)
	if RCMenu then RCMenu:Remove() end
	if int == MOUSE_RIGHT then
		//Display option menu
		local options = {}
		local inv = LocalPlayer():GetInventory()
		local itm = inv[self.slot]
		if itm == nil then return end
		//SPLIT
		if itm[2] > 1 then
			options[1] = {
				"Split",
				function() net.Start("inv_splititem") net.WriteString(self.slot) net.SendToServer() end,
			}
		end
		//USE & CONSUME
		if RUST.Inventory.Item[itm[1]].use then
			options[2] = {
				"Use",
				function() net.Start("inv_useitem") net.WriteString(self.slot) net.SendToServer() end,
			}
			if RUST.Inventory.Item[itm[1]].food then
				options[2][1] = "Consume"
			end
		end
		//UNLOAD
		if itm[3] && itm[3].bp_item then
			options[3] = {
				"Study",
				function() net.Start("inv_studyitem") net.WriteString(self.slot) net.SendToServer() end,
			}
		end
		//UNLOAD
		if RUST.Inventory.Item[itm[1]].gun then
			options[4] = {
				"Unload",
				function() net.Start("inv_unload") net.WriteString(self.slot) net.SendToServer() end,
			}
		end

		if #options == 0 then return end
		local frame = self:GetParent():GetParent()
		local x, y = frame:ScreenToLocal(gui.MousePos())
		RCMenu = vgui.Create("DPanel", frame)
		RCMenu:SetPos(x,y)
		RCMenu:SetSize(74, 20*table.Count(options))
		function RCMenu:Paint(w,h)
			draw.RoundedBox(2, 0, 0, w, h, Color(0, 0, 0))
		end
		local y = 1
		for k,v in pairs(options) do
			local btn = vgui.Create("DButton", RCMenu)
			btn:SetPos(1, y)
			btn:SetSize(72, 18)
			btn:SetText(v[1])
			btn:SetTextColor(Color(255,255,255))
			btn:SetFont("inventoryfont")
			local col = Color(48, 48, 48)
			local s = 10
			function btn:Paint(w, h)
				if self:IsHovered() then
					--draw.RoundedBox(0, 0, 0, w, h, Color(155, 255, 50))
					if Color(155, 255, 50) != col then
						col.r = math.Approach(col.r, 155, s)
						col.g = math.Approach(col.g, 255, s)
						col.b = math.Approach(col.b, 50, s)
					end
				else
					--draw.RoundedBox(0, 0, 0, w, h, Color(48, 48, 48))
					if Color(48, 48, 48) != col then
						col.r = math.Approach(col.r, 48, s)
						col.g = math.Approach(col.g, 48, s)
						col.b = math.Approach(col.b, 48, s)
					end
				end
				draw.RoundedBox(0, 0, 0, w, h, col)
			end
			btn.DoClick = function() v[2]() surface.PlaySound("shared/sfx/tap.wav") RCMenu:Remove() end
			y = y+20
		end
	end
end

local keytonum = {
	["slot1"] = 1,
	["slot2"] = 2,
	["slot3"] = 3,
	["slot4"] = 4,
	["slot5"] = 5,
	["slot6"] = 6,
}

hook.Add("PlayerBindPress", "inv_equip", function(ply, bind, pressed)
	local s = keytonum[bind]
	if !s then return end
	net.Start("inv_equipitem")
		net.WriteUInt(s, 3)
	net.SendToServer()
end)

function makeHotbar()
	hotbar_frame = vgui.Create("DFrame")
	hotbar_frame:SetSize(435, 64)
	hotbar_frame:SetPos(0, ScrH()-64-10)
	hotbar_frame:CenterHorizontal()
	hotbar_frame:SetTitle("22")
	hotbar_frame:SetDraggable(false)
	hotbar_frame:ShowCloseButton(false)
	function hotbar_frame:Paint(w,h) end
	local x, y = 0, 0
	for i=1,6 do
		local itmslot = vgui.Create("DPanel", hotbar_frame)
		itmslot:SetSize(64, 64)
		itmslot:SetPos(x, y)

		local itmi = vgui.Create("DPanel", itmslot)
		itmslot.itmi = itmi
		itmi:SetSize(itmslot:GetSize())
		itmi.slot = "h"..i
		itmi:Receiver("slot", function(pnl, tbl, dropped, n)
			if dropped then
				moveItem(tbl[1].slot, pnl.slot)
			end
		end)
		itmi:Droppable("slot")
		itmi.func = itmi.OnMousePressed
		function itmi:OnMousePressed(int)
			rightClick(self, int)
			if int == MOUSE_LEFT then
				self:func(int)
			end
		end
		function itmi:Paint(w, h)
			if LocalPlayer().GetInventory == nil then return end
			local tbl = LocalPlayer():GetInventory()
			if tbl == nil then return end
			if tbl[self.slot]!=nil then
				local itm, amt = tbl[self.slot][1], tbl[self.slot][2]
				--self:SetImage(RUST.Inventory.Item[self.item].icon)
				surface.SetDrawColor(255,255,255)
				surface.SetMaterial(RUST.Inventory.Item[itm].mat)
				surface.DrawTexturedRect(0, 0, 64, 64)
				if !self:IsDragging() then
					local w, h = surface.GetTextSize("x"..amt)
					surface.SetTextPos(64-w, 64-h+3)
					surface.DrawText("x")
					local w, h = surface.GetTextSize(amt)
					surface.SetTextPos(64-w+1, 64-h+2)
					surface.DrawText(amt)
				end
			end
		end
		function itmslot:Paint(w,h)
			if self.itmi:IsHovered() then
				draw.RoundedBox(2, 0, 0, w, h, Color(93, 93, 93))
			else
				draw.RoundedBox(2, 0, 0, w, h, Color(29, 29, 29))
			end
			draw.RoundedBoxEx(2, 63, 0, 1, h, Color(19, 19, 19), false, true, false, true)

			surface.SetFont("inventoryfont")
			surface.SetTextPos(2, 1)
			surface.SetTextColor(255,255,255)
			surface.DrawText(i)
		end
		x = x+64+10
	end
end

local armornames = {
	[1] = "HEAD",
	[2] = "CHEST",
	[3] = "LEGS",
	[4] = "BOOTS"
}

inv_x, inv_y = inv_x or nil, inv_y or nil
if inventory_frame then
	inventory_frame:Remove()
	armor_frame:Remove()
	hotbar_frame:Remove()
	crafting_frame:Remove()
end
if RCMenu then RCMenu:Remove() end
inventory_frame = nil // change to crafting_frame or nil when finished
armor_frame = nil
hotbar_frame = nil
crafting_frame = nil
RCMenu = nil
makeHotbar()
local function make_panel()
	inventory_frame = vgui.Create("DFrame")
	inventory_frame:SetSize(464, 414)
	inventory_frame:SetPos(0, ScrH()-95-415)
	inventory_frame:CenterHorizontal()
	inventory_frame:SetTitle("")
	inventory_frame:MakePopup()
	inventory_frame:SetDraggable(false)
	inventory_frame:ShowCloseButton(false)

	function inventory_frame:Paint(w,h)
		draw.RoundedBox(2, 0, 0, w, h, Color(47, 45, 46))
		draw.SimpleText("INVENTORY", "inventoryfont", 10, 5, Color(255,255,255))
	end

	function inventory_frame:OnKeyCodePressed(k)
		if k == KEY_TAB then
			inv_x, inv_y = gui.MousePos()
			self:Hide()
			armor_frame:Hide()
			crafting_frame:Hide()
		end
	end

	local ox, x, y = 8, 8, 40
	for i=1,30 do
		local itmslot = vgui.Create("DPanel", inventory_frame)
		itmslot:SetSize(64, 64)
		itmslot:SetPos(x, y)

		local itmi = vgui.Create("DPanel", itmslot)
		itmslot.itmi = itmi
		itmi:SetSize(itmslot:GetSize())
		itmi.slot = "s"..i
		itmi:Receiver("slot", function(pnl, tbl, dropped)
			if dropped then
				moveItem(tbl[1].slot, pnl.slot)
			end
		end)
		itmi:Droppable("slot")
		itmi.func = itmi.OnMousePressed
		function itmi:OnMousePressed(int)
			rightClick(self, int)
			if int == MOUSE_LEFT then
				self:func(int)
			end
		end
		function itmi:Paint(w, h)
			local tbl = LocalPlayer():GetInventory()
			if tbl[self.slot]!=nil then
				local itm, amt = tbl[self.slot][1], tbl[self.slot][2]
				--self:SetImage(RUST.Inventory.Item[self.item].icon)
				surface.SetDrawColor(255,255,255)
				surface.SetMaterial(RUST.Inventory.Item[itm].mat)
				surface.DrawTexturedRect(0, 0, 64, 64)
				if !self:IsDragging() then
					surface.SetFont("inventoryfont")
					surface.SetTextColor(255,255,255)
					local w, h = surface.GetTextSize("x"..amt)
					surface.SetTextPos(64-w, 64-h+3)
					surface.DrawText("x")
					local w, h = surface.GetTextSize(amt)
					surface.SetTextPos(64-w+1, 64-h+2)
					surface.DrawText(amt)
				end
			end
		end

		local fl = math.floor(i/6)
		y = 40 + ((64 + 10)*(fl))
		x = x+64+10
		if x+64 >= inventory_frame:GetWide() then
			x = ox
		end
		function itmslot:Paint(w,h)
			if self.itmi:IsHovered() then
				draw.RoundedBox(2, 0, 0, w, h, Color(93, 93, 93))
			else
				draw.RoundedBox(2, 0, 0, w, h, Color(29, 29, 29))
			end
			draw.RoundedBoxEx(2, 63, 0, 1, h, Color(19, 19, 19), false, true, false, true)
		end
	end

	armor_frame = vgui.Create("DFrame")
	armor_frame:SetSize(214, 324)
	x,y = inventory_frame:GetPos()
	armor_frame:SetPos(x-armor_frame:GetWide()-4, y)
	armor_frame:SetTitle("")
	armor_frame:SetDraggable(false)
	armor_frame:ShowCloseButton(false)
	function armor_frame:Paint(w,h)
		draw.RoundedBox(2, 0, 0, w, h, Color(47, 45, 46))
		draw.SimpleText("ARMOR", "inventoryfont", 10, 5, Color(255,255,255))
	end

	local x, y = 14, 41
	for i=1,4 do
		local itmslot = vgui.Create("DPanel", armor_frame)
		itmslot:SetSize(64, 64)
		itmslot:SetPos(x, y)

		local itmi = vgui.Create("DPanel", itmslot)
		itmslot.itmi = itmi
		itmi:SetSize(itmslot:GetSize())
		itmi.slot = "c"..i
		itmi:Receiver("slot", function(pnl, tbl, dropped, n)
			if dropped then
				moveItem(tbl[1].slot, pnl.slot)
			end
		end)
		itmi:Droppable("slot")
		itmi.func = itmi.OnMousePressed
		function itmi:OnMousePressed(int)
			rightClick(self, int)
			if int == MOUSE_LEFT then
				self:func(int)
			end
		end
		function itmi:Paint(w, h)
			local tbl = LocalPlayer():GetInventory()
			if tbl[self.slot]!=nil then
				local itm, amt = tbl[self.slot][1], tbl[self.slot][2]
				--self:SetImage(RUST.Inventory.Item[self.item].icon)
				surface.SetDrawColor(255,255,255)
				surface.SetMaterial(RUST.Inventory.Item[itm].mat)
				surface.DrawTexturedRect(0, 0, 64, 64)
			end
		end
		surface.SetFont("inventoryfont")
		local _w, _h = surface.GetTextSize(armornames[i])
		function itmslot:Paint(w,h)
			if self.itmi:IsHovered() then
				draw.RoundedBox(2, 0, 0, w, h, Color(93, 93, 93))
			else
				draw.RoundedBox(2, 0, 0, w, h, Color(29, 29, 29))
			end
			draw.RoundedBoxEx(2, 63, 0, 1, h, Color(19, 19, 19), false, true, false, true)
			draw.RoundedBoxEx(2, 0, 63, w, 1, Color(19, 19, 19), true, true)
			draw.DrawText(armornames[i], "inventoryfont", 32-(_w/2), 32-(_h/2), Color(50, 50, 50)) // 4 is half the size of the font
		end
		y = y + 4 + 64
	end

	crafting_frame = vgui.Create("DFrame")
	crafting_frame:SetSize(244, 324)
	x,y = inventory_frame:GetPos()
	crafting_frame:SetPos(x+inventory_frame:GetWide()+6, y+2)
	crafting_frame:SetTitle("")
	crafting_frame:SetDraggable(false)
	crafting_frame:ShowCloseButton(false)
	function crafting_frame:Paint(w,h)
		draw.RoundedBox(2, 0, 0, w, h, Color(47, 45, 46))
		draw.SimpleText("CRAFTING", "inventoryfont", 8, 1, Color(255,255,255))
	end

	local cs = vgui.Create("DScrollPanel", crafting_frame)
	cs:SetPos(6, 37)
	cs:SetSize(282, 477)

	local last_cat = ""
	local selected_btn
	local y = 0
	for k,v in pairs(RUST.Inventory.Recipes) do
		if last_cat != v.category then
			local divider = cs:Add("DPanel")
			divider:SetSize(221, 14)
			divider:SetPos(0, y)
			function divider:Paint(w, h)
				draw.RoundedBox(0, 0, 0, w, h, Color(29, 29, 29))
				draw.SimpleText(v.category, "inventoryfont", 10, 1, Color(255,255,255))
			end
			y = y + 14 + 2
		end
		// button
		local btn = cs:Add("DButton")
		btn:SetSize(207, 14)
		btn:SetPos(10, y)
		btn:SetText("")
		function btn:Paint(w, h)
			if self:IsHovered() then
				draw.RoundedBox(0, 0, 0, w, h, Color(126, 126, 126))
			else
				draw.RoundedBox(0, 0, 0, w, h, Color(29, 29, 29))
			end
			surface.SetFont("inventoryfont")
			surface.SetTextPos(10, 1)
			if selected_btn == self then
				surface.SetTextColor(Color(255,235,5))
			else
				surface.SetTextColor(Color(255,255,255))
			end
			surface.DrawText(RUST.Inventory.Item[v.result[1]].name)
			--draw.SimpleText(RUST.Inventory.Item[v.result[1]].name, "inventoryfont", 10, 1, Color(255,255,255))
		end
		function btn:DoClick()
			selected_btn = self
			surface.PlaySound("shared/sfx/tap.wav")
		end
		y = y + 14 + 2
	end

	armor_frame:Hide()
	inventory_frame:Hide()
	crafting_frame:Hide()
end

if inventory_frame == nil then make_panel() end
function GM:ScoreboardShow()
	if inventory_frame == nil then make_panel() end
	inventory_frame:SetVisible(true) // this function wont be called when ur in the ui, meaning it can only show
	armor_frame:SetVisible(true)
	crafting_frame:SetVisible(true)
	if inv_x != nil then input.SetCursorPos(inv_x, inv_y) end
end