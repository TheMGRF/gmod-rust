RUST.Inventory.Recipes = {}
local function AddCraftingRecipe(cat, t, itc, res) -- Category(string, name), Time(int, seconds), ItmsToCraft(table, {{"item_id", amount}, {"item_id", amount}}, Result(table, {"item_id", amount})
	local i = table.insert(RUST.Inventory.Recipes, {})
	RUST.Inventory.Recipes[i].category = cat
	RUST.Inventory.Recipes[i].time = time
	RUST.Inventory.Recipes[i].needed = itc
	RUST.Inventory.Recipes[i].result = res
end

AddCraftingRecipe("Survival", 10, {}, {"cooked_chicken_breast", 1})
AddCraftingRecipe("Weapon", 10, {}, {"rust_m4", 1})